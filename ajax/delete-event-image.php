<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/EventImage.php';

if (empty($_SESSION['IsAdmin'])) exit;

$event_image_service = new EventImage();

$imgID = (int)$_POST['imgID'];

$event_image_service->deleteByID($imgID);