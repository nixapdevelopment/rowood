<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Review.php';
require_once DOC_ROOT . '/services/ReviewLang.php';

$review_service = new Review();
$review_lang_service = new ReviewLang();

$review = $review_service->getWhere(['ID'=> Request::post('ReviewID')]);



$review_service->delete(['ID'=> Request::post('ReviewID')]);
$review_lang_service->delete(['ReviewID'=> Request::post('ReviewID')]);
    
echo '<div class="alert alert-success">Recenzie a fost stearsa!</div>';
