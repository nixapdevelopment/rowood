<?php

require_once '../includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/libraries/Email.php';

$name = Request::post('name');
$email = Request::post('email');
$phone = Request::post('phone');
$message = Request::post('message');

ob_start();
?>
<table>
    <tr>
        <td>Nume</td>
        <td><?= $name ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?= $email ?></td>
    </tr>
    <tr>
        <td>Telefon</td>
        <td><?= $phone ?></td>
    </tr>
    <tr>
        <td>Mesaj</td>
        <td><?= $message ?></td>
    </tr>
</table>
<?php $message = ob_get_clean();

Email::send('colesnic89@gmail.com', 'COMANDA UN APEL', $message);