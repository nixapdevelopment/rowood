<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/PageImage.php';

if (empty($_SESSION['IsAdmin'])) exit;

$page_image_service = new PageImage();

$imgID = (int)$_POST['imgID'];

$page_image_service->deleteByID($imgID);