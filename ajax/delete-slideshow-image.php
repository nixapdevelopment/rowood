<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/SlideShow.php';

if (empty($_SESSION['IsAdmin'])) exit;

$slideshow_service = new SlideShow();

$imgID = (int)$_POST['imgID'];

$img = $slideshow_service->getByID($imgID);

$slideshow_service->deleteByID($imgID);

if($img['Type']=='foto'){
    @unlink(DOC_ROOT . '/uploads/slideshow/'.$img['Url']);
    @unlink(DOC_ROOT . '/uploads/slideshow/'.$img['Thumb_url']);
}