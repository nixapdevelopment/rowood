<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Offer.php';

$offer_service = new Offer();

if (Request::isPost())
{
    $valid = false;
    
    $name = Validator::validate('Name', Validator::ValidateEmpty, 'Nume, Prenume');
    $email = Validator::validate('Email', Validator::ValidateEmail, 'Email');
    $phone = Request::post('Phone');
    $categotyID = (int)Request::post('Category');
    $message = strip_tags(Request::post('Message'));
    
    if (!Validator::hasErrors())
    {
        $offer_service->insert([
            'CategoryID' => $categotyID,
            'Name' => $name,
            'Email' => $email,
            'Phone' => $phone,
            'Message' => $message
        ]);
        
        Validator::setSuccess('Success');
        $valid = true;
    }
    
    exit(json_encode([
        'messages' => Validator::showMessages(),
        'valid' => $valid
    ]));
}