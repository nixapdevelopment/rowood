<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/CategoryImage.php';

if (empty($_SESSION['IsAdmin'])) exit;

$category_image_service = new CategoryImage();

$imgID = (int)$_POST['imgID'];

$category_image_service->deleteByID($imgID);