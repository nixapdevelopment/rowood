<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/SlideShow.php';

if (empty($_SESSION['IsAdmin'])) exit;

$slideshow_service = new SlideShow();

if(isset($_POST['sortedIDs'])){
    $slide_sort = json_decode($_POST['sortedIDs'], true);

    foreach ($slide_sort as $sid => $sval){        
            $sql[1] = ['Sort' => $sid];
            $sql[2] = ['ID' => $sval];
            $slideshow_service->update($sql[1], $sql[2]);
               
    }
}





