<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Review.php';

$review_service = new Review();
$review_ids =  Request::post('ID');

foreach ($review_ids as $key => $pID)
{    
    $review_service->update(['Sort'=> $key], ['ID'=>$pID]);
}

echo '<div class="alert alert-success">Sortare Salvata!</div>';

?>