<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Subscribe.php';

$subscribe_service = new Subscribe();

if (Request::isPost())
{
    $email = Validator::validate('Email', Validator::ValidateEmail, 'Email');
    
    if (!Validator::hasErrors())
    {
        $result = $subscribe_service->getByEmail($email);
        
        if (!empty($result))
        {
            Validator::setError('Acest email este deja abonat');
        }
        
        if (!Validator::hasErrors())
        {
            $subscribe_service->insert([
                'Email' => $email
            ]);
        }
    }
    
    exit(json_encode([
        'errors' => Validator::showMessages(true)
    ]));
}