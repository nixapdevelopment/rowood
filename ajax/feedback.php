<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Feedback.php';

$feedback_service = new Feedback();

if (Request::isPost())
{
    $fname = Validator::validate('FirstName', Validator::ValidateEmpty, 'Nume');
    $lname = Validator::validate('LastName', Validator::ValidateEmpty, 'Prenume');
    $email = Validator::validate('Email', Validator::ValidateEmail, 'Email');
    $phone = Validator::validate('Phone', Validator::ValidateEmpty, 'Telefon');
    $message = Validator::validate('Message', Validator::ValidateEmpty, 'Mesaj');
    
    if (!Validator::hasErrors())
    {
        $feedback_service->insert([
            'FirstName' => $fname,
            'LastName' => $lname,
            'Email' => $email,
            'Phone' => $phone,
            'Message' => $message
        ]);
    }
    
    exit(json_encode([
        'errors' => Validator::showMessages(true)
    ]));
}