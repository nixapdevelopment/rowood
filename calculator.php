<div class="row">
    <div class="col-md-9">
        <h4>Тип сайта</h4>
        <div class="btn-group" role="group" aria-label="...">
            <label type="button" class="btn btn-default">
                <input type="radio" name="type" value="" /> Лэндинг
            </label>
            <label type="button" class="btn btn-default">
                <input type="radio" name="type" value="" /> Визитка
            </label>
            <label type="button" class="btn btn-default">
                <input type="radio" name="type" value="" /> Корпоративный
            </label>
            <label type="button" class="btn btn-default">
                <input type="radio" name="type" value="" /> Магазин
            </label>
        </div>
        <hr />
        <h4>Модули</h4>
        <div class="row">
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Новости
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="promotions"> Акции
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name=""> Каталог
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Наши работы
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Отзывы
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Клиенты
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Онлайн-заказ
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Онлайн-оплата
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Поиск
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Калькулятор
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Документы
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="news"> Вопрос - Ответ
                    </label>
                </div>
            </div>
        </div>
        <hr />
        <h4>Наполнение</h4>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Создание и наполнение страниц</label>
                    <input type="number" class="form-control" name="pages" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Количество языков</label>
                    <input type="number" class="form-control" name="languages" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Размещение изображений</label>
                    <input type="number" class="form-control" name="images" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        
    </div>
</div>