<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Page.php';
require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/services/Service.php';
require_once DOC_ROOT . '/services/PageImage.php';

$page_service = new Page();
$category_service = new Category();
$service_service = new Service();
$page_image_service = new PageImage();

$current_article = $page_service->getByID(3);

$services = $service_service->getAll();

$rand_categories = $category_service->getAllWithImages(8, true);


$images = $page_image_service->getWhere([
    'PageID' => $current_article['ID']
]);

$TITLE = $current_article['Title'];
$KEYWORDS = $current_article['Keywords'];
$DESCRIPTION = $current_article['Description'];

?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<section class="home-indigo indigo-mini"></section>

<section class="content">
    <div class="bread">		
        <div class="container">
            <a href=""><i class="fa fa-home"></i> Home</a> - Serviciu
        </div>
    </div>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title"><?= $current_article['Title'] ?> <div class="big-dot-white"></div></h2>
                <div>
                    <?= $current_article['Text'] ?>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($services as $item) { ?>
            <div class="service-item col-md-4">
                <img src="<?= Url::link('uploads/' . $item['Image']) ?>" />
                <h3><?= $item['Title'] ?></h3>
                <p><?= $item['Text'] ?></p>
            </div>
            <?php } ?>
        </div>
        <br />
        <div class="row">
            <?php foreach ($images as $item) { ?>
            <div class="col-md-3">
                <div style="margin: 15px 0;" class="portfolio-item">
                    <a role="images" class="fancybox" href="<?= Url::link('uploads/' . $item['Image']) ?>">
                        <div class="portfolio-thumb">
                        <img src="<?= Url::link('uploads/' . $item['Thumb']) ?>" alt="<?= $item['Alt'] ?>" />
                        </div>
                    </a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>	
</section>

<section class="promotii violet-pattern">
    <div class="container">
        <h4>În fiecare lună te așteptăm cu promoții</h4>
        <h2 class="white-title">Promoții noi <div class="big-dot"></div></h2>
        <p>
            Lorem ipsum dolor sit amet, sanctus intellegam cu sit, his quis alii falli eu, est no posse timeam. Cu populo fabellas vix, te tantas iuvaret nam. Vidit solet vulputate sed te, ad pri essent eirmod invidunt, eos singulis maluisset in. Accusata gloriatur an quo, ius exerci feugait phaedrum an, affert disputationi vituperatoribus pri te.
        </p>
        <a href="#" data-toggle="modal" data-target="#offer-modal" class="cere-oferta">Cere oferta</a>
    </div>
</section>

<section class="home-portfolio">
    <div class="container">
        <div class="row">
            <?php foreach ($rand_categories as $item) { ?>
            <div class="portfolio-item col-md-3">
                <a href="<?= Url::route($item['Link']) ?>" title="">
                    <div class="portfolio-thumb">
                        <img src="<?= Url::link('uploads/' . $item['Thumb']) ?>" alt="" />
                        <p><?= $item['Title'] ?></p>
                    </div>
                </a>
            </div>
            <?php } ?>	
        </div>
    </div>
</section>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>