$(document).ready(function(){

    if($(window).width() >= 481 ) {
        new WOW().init();
    }

    $(".item_wrapper").matchHeight();

    //Lightbox gallery
    $('.gallery_wrapp').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        zoom: {
            enabled: true, // By default it's false, so don't forget to enable it
            duration: 300, // duration of the effect, in milliseconds
            easing: 'ease-in-out' // CSS transition easing function
        }
    });

    $("#go_top").on("click", function (e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, 400);
    });

    var owl_home = $("#owl_home");
    owl_home.owlCarousel({
        loop: true,
        mouseDrag: false,
        navigation : true,
        singleItem : true,
        transitionStyle : "fade",
        autoPlay: 6000,
        //autoPlay: false,
        stopOnHover : false
    });

    var owl_rews = $("#owl_rews");
    owl_rews.owlCarousel({
        loop: true,
        mouseDrag: false,
        navigation : true,
        singleItem : true,
        transitionStyle : "fade",
        //autoPlay: 6000,
        autoPlay: false,
        stopOnHover : false
    });



    // Popup modal
    $(".clickUP").magnificPopup({
        type:"inline",
        midClick: true,
        mainClass: 'default'
    });

    $(".menu_switcher").on("click", function(e){
        e.preventDefault();
    });


    function showPopupCall(){
        if ($('#call').length) {
            $.magnificPopup.open({
                items: {
                    src: '#call'
                },
                type: 'inline'
            });
        }
    }

    $(".menu_wrapper .menu_toggle span.more").on("click", function(e){
        e.preventDefault();
        $(this).toggleClass("active");
        $(".menu_wrapper .menu_toggle span.less").toggleClass("active");
    });

    $(".menu_wrapper .menu_toggle span.less").on("click", function(e){
        e.preventDefault();
        $(this).toggleClass("active");
        $(".menu_wrapper .menu_toggle span.more").toggleClass("active");
    });

    $('#mixes').mixitup();

    $(".tab_mix a").on("click", function(e){
        e.preventDefault();
    }); 
    
    var hash = window.location.hash;
    hash = hash === '' ? 'all' : ('category_' + hash);
    hash = hash.replace('#', '');
    $('a[data-filter="' + hash + '"]').click().trigger('click');
    
    $(".thumb_slider .bxslider").bxSlider({
        pagerCustom: '#bx-pager',
        infiniteLoop: true,
        pager:true,
        controls: false,
        slideWidth: 570,
        maxSlides: 1,
        minSlides: 1,
        slideMargin: 0,
        moveSlides: 1
    });
    
    $('.call_form').submit(function(e){
        e.preventDefault();
        $.post('/ajax/call-form.php', $(this).serialize(), function(){
            $('.call_form').html('<h2 class="uk-text-center">Multumim</h2>');
        });
    });


}); //# READY


