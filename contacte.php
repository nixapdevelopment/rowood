﻿<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Page.php';
require_once DOC_ROOT . '/services/Category.php';

$page_service = new Page();
$category_service = new Category();

$current_article = $page_service->getByID(2);

$rand_categories = $category_service->getAllWithImages(4, true);

$TITLE = $current_article['Title'];
$KEYWORDS = $current_article['Keywords'];
$DESCRIPTION = $current_article['Description'];

?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<br />
<br />
<br />
<div class="uk-clearfix section">
    <div class="uk-container uk-container-center page_content">
        <div class="heading wow fadeInUp">
            <h4>
                <span>
                    <?= $current_article['Title'] ?>
                </span>
            </h4>
            <h2>
                <?= $current_article['Title'] ?>
            </h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title"><?= $current_article['Title'] ?> <div class="big-dot-white"></div></h2>
                <p><?= $current_article['Text'] ?></p>
                <p>&nbsp;</p>
                <div class="row">
                    <div class="service-item col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="contact-icon"><i class="fa fa-phone"></i></div>
                            </div>
                            <div class="col-md-9">								
                                <h3>Telefon</h3>
                                <div class="clearfix"></div>
                                <p>
                                    0729 463 446
                                </p>
                            </div>
                        </div>
                    </div>	
                    <div class="service-item col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="contact-icon"><i class="fa fa-envelope"></i></div>
                            </div>
                            <div class="col-md-9">								
                                <h3>Telefon</h3>
                                <div class="clearfix"></div>
                                <p>
                                    <a href="mailto:office@indigoneon.ro">office@indigoneon.ro</a>
                                </p>
                            </div>
                        </div>
                    </div>	
                    <div class="service-item col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="contact-icon"><i class="fa fa-map-marker"></i></div>
                            </div>
                            <div class="col-md-9">								
                                <h3>Adresa</h3>
                                <div class="clearfix"></div>
                                <p>
                                    Sector 6, Bucuresti<br/>
				Orar: 09:00 - 18:00
                                </p>
                            </div>
                        </div>
                    </div>						
                </div>
            </div>
        </div>

        <div class="contact-form green-pattern">
            <h4 class="sub-title">Trimite un mesaj</h3>
            <p><!--Vis id posse fuisset appellantur, id duo ipsum reformidans. Eu eos denique constituam. Graece electram repudiare et nam. Sale scripta pri id, dico primis labitur ei pro. Id solet delenit honestatis pro, ut qui porro legere conceptam.--></p>
            <form id="feedback-form">
                <div class="errors" style="color: #ffccba; font-weight: 600; margin-bottom: 5px;"></div>
                <div class="success" style="color: #00ff59; font-weight: 600; margin-bottom: 5px;"></div>
                <input required type="text" name="FirstName" placeholder="Nume *" />
                <input required type="text" name="LastName" placeholder="Prenume *" />
                <input required type="email" name="Email" placeholder="Email *" />
                <input required type="tel" name="Phone" placeholder="Telefon *" />
                <textarea required name="Message" placeholder="Mesaj *"></textarea>
                <div class="">
                    <div class="text-left col-md-9">
                        <br />
                        <br />
                        Campuri obligatorii
                        <br />
                        <br />
                    </div>
                    <div class="col-md-3">
                        <button class="pull-right cere-oferta">Trimite</button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>	
</div>

<section class="promotii violet-pattern">
    <div class="container">
        <h4>În fiecare lună te așteptăm cu promoții</h4>
        <h2 class="white-title">Promoții noi <div class="big-dot"></div></h2>
        <p>
          <!--  Lorem ipsum dolor sit amet, sanctus intellegam cu sit, his quis alii falli eu, est no posse timeam. Cu populo fabellas vix, te tantas iuvaret nam. Vidit solet vulputate sed te, ad pri essent eirmod invidunt, eos singulis maluisset in. Accusata gloriatur an quo, ius exerci feugait phaedrum an, affert disputationi vituperatoribus pri te. -->
        </p>
        <a href="#" data-toggle="modal" data-target="#offer-modal" class="cere-oferta">Cere oferta</a>
    </div>
</section>

<section class="home-portfolio">
    <div class="container">
        <div class="row">
            <?php foreach ($rand_categories as $item) { ?>
            <div class="portfolio-item col-md-3">
                <a href="<?= Url::route('catalog/' . $item['Link']) ?>" title="">
                    <div class="portfolio-thumb">
                        <img src="<?= Url::link('uploads/' . $item['Thumb']) ?>" alt="" />
                        <p><?= $item['Title'] ?></p>
                    </div>
                </a>
            </div>
            <?php } ?>	
        </div>
    </div>
</section>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>