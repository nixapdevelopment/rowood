<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/services/Page.php';
require_once DOC_ROOT . '/services/News.php';
require_once DOC_ROOT . '/services/Event.php';
require_once DOC_ROOT . '/services/SlideShow.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';
require_once DOC_ROOT . '/libraries/Lang.php';
require_once DOC_ROOT . '/services/Review.php';


$review_service = new Review();

$category_service = new Category();
$page_service = new Page();
$news_service = new News();
$event_service = new Event();
$slideshow_service = new SlideShow();

$left_slides = $slideshow_service->getAll('WHERE Position = "left"');

$newslist = $news_service->getAll('ORDER BY Date desc LIMIT 8');

$page = $page_service->getAll("WHERE p.ID = 1 LIMIT 1");
$page = reset($page);

$about_page = $page_service->getAll("WHERE p.ID = 4 LIMIT 1");
$about_page = reset($about_page);

$menu_page = $page_service->getAll();

$categories = $category_service->getAll();

$reviews = $review_service->getAll("ORDER BY `Sort`");

$res = $page_service->getAll('WHERE p.ID = 7 LIMIT 1');
$products_page = reset($res);

$TITLE = $page['Title'];
$KEYWORDS = $page['Keywords'];
$DESCRIPTION = $page['Description'];
?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<div class="section">
    <div class="container">
        <div class="uk-clearfix page_content">
            <div class="heading wow fadeInUp">
                <h4>
                    <span>
                        <?= $page['Title'] ?>
                    </span>
                </h4>
                <h2>
                    <?= $page['Title'] ?>
                </h2>
            </div>

            <div class="uk-text-center">
                <?= $page['Text'] ?>
                <p>
                    <a href="<?= Url::route('p/' . $about_page['Link']) ?>" class="btn white wow tada">Detalii</a>
                </p>
            </div>
        </div>
    </div>
</div>

<!--div class="section gray">
    <div class="uk-container uk-container-center">
        <div class="uk-clearfix page_content">
            <div class="heading wow fadeInUp">
                <h4>
                    <span>
                        Produse
                    </span>
                </h4>
                <h2>
                    Produse <span>Rowood</span>
                </h2>
            </div>

            <div class="uk-clearfix products">
                <?php foreach ($categories as $key => $category) { ?>
                    <?php if ($key % 2 == 0) { ?>
                        <div class="table">
                            <div class="middle item">
                                <div class="uk-clearfix">
                                    <div class="title">
                                        <h2>
                                            <?= $category['Title'] ?>
                                        </h2>
                                        <span></span>
                                    </div>
                                    <div class="content">
                                        <h4>
                                            <?= $category['Title'] ?>
                                        </h4>
                                        <p>
                                            <?= mb_substr(strip_tags($category['Text']), 0, 200) ?>
                                        </p>

                                        <div class="table auto buttons">
                                            <div class="middle">
                                                <a href="#call" class="btn black clickUP">Comanda</a>
                                            </div>
                                            <div class="middle">
                                                <a href="<?= Url::route('p/' . $products_page['Link'] . '#' . $category['ID']) ?>" class="btn white">Detalii</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="middle">
                                <img class="wow fadeIn" src="<?= Url::link('uploads/' . $category['Image']) ?>" alt="<?= $category['Title'] ?>" />
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="table">
                            <div class="middle ">
                                <img class="wow fadeIn"  src="<?= Url::link('uploads/' . $category['Image']) ?>" alt="<?= $category['Title'] ?>" />
                            </div>
                            <div class="middle item">
                                <div class="uk-clearfix">
                                    <div class="title">
                                        <h2>
                                            <?= $category['Title'] ?>
                                        </h2>
                                        <span></span>
                                    </div>
                                    <div class="content">
                                        <h4>
                                            <?= $category['Title'] ?>
                                        </h4>
                                        <p>
                                            <?= mb_substr(strip_tags($category['Text']), 0, 200) ?>
                                        </p>

                                        <div class="table auto buttons">
                                            <div class="middle">
                                                <a href="#call" class="btn black clickUP">Comanda</a>
                                            </div>
                                            <div class="middle">
                                                <a href="<?= Url::route('p/' . $products_page['Link'] . '#' . $category['ID']) ?>" class="btn white">Detalii</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div-->

<?php require_once DOC_ROOT . '/template/footer.php'; ?>