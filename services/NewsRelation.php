<?php

require_once 'Database.php';

class NewsRelation extends Database
{
    
    public function getTableName()
    {
        return 'NewsRelation';
    }
    
    
    public function getNewsRelationByID($newsID, $type)
    {
        $newsID = (int)$newsID;
        $sql = "SELECT * FROM `" . $this->getTableName() . "` WHERE `NewsID` = $newsID && Type = '$type'";
        
        $res = $this->query($sql);
                        
        $ids = [];
        foreach ($res as $row)
        {
            $ids[] = $row['ObjectID'];
        }
        
        return $ids;
    }   
    
    public function getRelatedNews($eventID)
    {
        $eventID = (int)$eventID;
        $sql = "SELECT * FROM `" . $this->getTableName() . "` as nr left join NewsLang as nl on nl.NewsID = nr.NewsID and nl.LangID = " . Request::getLangID() . " WHERE `ObjectID` = $eventID and Type = 'Event'";
        
        return $this->query($sql);

    }
    

    public function deleteRelations($newsID)
    {
        $newsID = (int)$newsID;
        
        return $this->query("delete from `" . $this->getTableName() . "` where `NewsID` = $newsID");
    }   
    
    
}