<?php

require_once 'Database.php';

class News extends Database
{
    
    public function getTableName()
    {
        return 'News';
    }
    
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link
        ], 1);
        
        return reset($res);
    }
    
    public function getAll($addon = '')
    {
        return $this->query("select * FROM News as n left join NewsLang as nl on nl.NewsID = n.ID and nl.LangID = " . Request::getLangID() . " $addon");
    }
    
}