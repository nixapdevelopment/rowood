<?php

require_once 'Database.php';

class EventNews extends Database
{
    
    public function getTableName()
    {
        return 'EventNews';
    }
    
    public function getByProductID($productID)
    {
        $productID = (int)$productID;
        
        return $this->query("
            SELECT * FROM `EventNews` AS en 
            LEFT JOIN `News` AS n ON n.ID = en.NewsID 
            LEFT JOIN `NewsLang` AS nl ON nl.NewsID = n.ID AND nl.LangID = " . Request::getLangID() . " 
            WHERE en.EventID = $productID
        ");
    }
    
}