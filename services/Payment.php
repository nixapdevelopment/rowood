<?php

require_once 'Database.php';

class Payment extends Database
{
    
    const StatusPending = 'Pending';
    const StatusPaid = 'Paid';
    const StatusCanceled = 'Canceled';

    public function getTableName()
    {
        return 'Payment';
    }
    
}