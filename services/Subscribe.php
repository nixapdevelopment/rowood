<?php

require_once 'Database.php';

class Subscribe extends Database
{
    
    public function getTableName()
    {
        return 'Subscribe';
    }
    
    public function getByEmail($email)
    {
        $res = $this->getWhere([
            'Email' => $email
        ], 1);
        
        return reset($res);
    }
    
}