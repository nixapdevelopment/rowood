<?php

require_once 'Database.php';

class Event extends Database
{
    
    public function getTableName()
    {
        return 'Event';
    }
    
    public function getByLink($link)
    {
        $link = $this->escape($link);
        
        $res = $this->query("select *, e.ID, cl.Title as CategoryName FROM "  . $this->getTableName() . " as e 
            left join Category as c on c.ID = e.CategoryID 
            left join CategoryLang as cl on c.ID = cl.CategoryID and cl.LangID = " . Request::getLangID() . " 
            left join EventImage as ei on ei.EventID = e.ID and ei.Main = 1                 
            left join EventLang as el on el.EventID = e.ID and el.LangID = " . Request::getLangID() . "
            WHERE el.Link = '$link' 
            LIMIT 1
        ");
        
        return reset($res);
    }
    
    public function getAll($addon = '')
    {
        return $this->query("select *, cl.Title as CategoryName FROM "  . $this->getTableName() . " as e 
            left join Category as c on c.ID = e.CategoryID 
            left join CategoryLang as cl on c.ID = cl.CategoryID and cl.LangID = " . Request::getLangID() . " 
            left join EventImage as ei on ei.EventID = e.ID and ei.Main = 1                 
            left join EventLang as el on el.EventID = e.ID and el.LangID = " . Request::getLangID() . " $addon
        ");
    }
    
    public function getList()
    {
        $res = $this->query("select *, e.ID, el.Title, cl.Title as CategoryName, einf.Icon as InfoIcon, einf.Title as InfoTitle, einf.Subtitle as InfoSubtitle FROM "  . $this->getTableName() . " as e 
            left join Category as c on c.ID = e.CategoryID 
            left join CategoryLang as cl on c.ID = cl.CategoryID and cl.LangID = " . Request::getLangID() . " 
            left join EventImage as ei on ei.EventID = e.ID and ei.Main = 1                 
            left join EventLang as el on el.EventID = e.ID and el.LangID = " . Request::getLangID() . "
            left join EventInfo as einf on einf.EventID = e.ID and einf.LangID = " . Request::getLangID() . "
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['ID']]['data'] = $row;
            if (!empty($row['InfoIcon']))
            {
                $return[$row['ID']]['info'][] = $row;
            }
        }
        
        return $return;
    }
    
}