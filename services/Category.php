<?php

require_once 'Database.php';

class Category extends Database
{
    
    public function getTableName()
    {
        return 'Category';
    }
    
    public function getAllWithImages($limit = false, $rand = false)
    {
        return $this->query("
            select * from Category as c 
            /*left join CategoryLang as cl on cl.CategoryID = c.ID and cl.LangID = " . Request::getLangID() . " */
            left join CategoryImage as ci on ci.CategoryID = c.ID and ci.Main = 1 
            where ci.ID is not null 
            " . ($rand ? ' order by rand()' : '') . " 
            " . ($limit ? (' limit ' . $limit) : '') . " 
        ");
    }
    
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link,
            'LangID' => Request::getLangID()
        ], 1);

        return reset($res);
    }
    
    public function getAll($addon = '')
    {
        return $this->query("select *, c.ID FROM Category as c left join CategoryLang as cl on cl.CategoryID = c.ID and cl.LangID = " . Request::getLangID() . " left join CategoryImage as ci on ci.CategoryID = c.ID and ci.Main = 1 $addon");
    }
    
    public function getTree($langID, $frontend = false, $categoryPage = false)
    {
        $categories = $this->getAll();
        
        $tree = $this->categoriesToTree($categories);
        
        return $frontend ? $this->buildFrontendTree($tree, true, $categoryPage) : $this->buildTree($tree);
    }
            
    public function categoriesToTree(&$categories)
    {
        $map = array(
            0 => array('subcategories' => array())
        );

        foreach ($categories as &$category)
        {
            $category['subcategories'] = array();
            $map[$category['ID']] = &$category;
        }
        
        foreach ($categories as &$category)
        {
            $map[$category['ParentID']]['subcategories'][] = &$category;
        }

        return $map[0]['subcategories'];
    }
    
    private function buildTree($array)
    {
        $return = '<ul class="cat-admin-view">';
        foreach ($array as $row)
        {
            $return .= '<li cat-id="' . $row['ID'] . '"><span>' . $row['Title'] . "</span><a href=" . Url::route('admin/categories/edit.php', ['id' => $row['ID']]) . " class=\"btn btn-warning btn-sm\"><i class=\"glyphicon glyphicon-pencil\"></i> Edit</a>
                        <a style=\"margin-left: 1px;\" href=" . Url::route('admin/categories/index.php', ['delID' => $row['ID']]) . " onclick=\"return confirm('Confirmați?')\" class=\"btn btn-danger btn-sm\"><i class=\"glyphicon glyphicon-trash\"></i> Delete</a>";
            if (count($row['subcategories']) > 0)
            {
                $return .= $this->buildTree($row['subcategories']);
            }
            $return .= '</li>';
        }
        $return .= '</ul>';
        
        return $return;
    }
    
    public function getRelatedNews($categoryID)
    {
        $categoryID = (int)$categoryID;
        
        $res = $this->query("
            SELECT * FROM `NewsRelation` as nr 
            left join News as n on n.ID = nr.NewsID 
            left join NewsLang as nl on nl.NewsID = n.ID and nl.LangID = " . Request::getLangID() . " 
            WHERE nr.`ObjectID` = $categoryID AND  nr.`Type` = 'Category' 
        ");
        
        return $res;
    }
    
}