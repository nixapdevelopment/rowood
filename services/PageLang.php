<?php

require_once 'Database.php';

class PageLang extends Database
{
    
    public function getTableName()
    {
        return 'PageLang';
    }    
        
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link,
            'LangID' => Request::getLangID()
        ], 1);
        
        return reset($res);
    }
    
}