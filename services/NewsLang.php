<?php

require_once 'Database.php';

class NewsLang extends Database
{
    
    public function getTableName()
    {
        return 'NewsLang';
    }    
        
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link,
            'LangID' => Request::getLangID()
        ], 1);
        
        return reset($res);
    }   
    
}