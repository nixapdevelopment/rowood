<?php

require_once 'Database.php';

class CategoryLang extends Database
{
    
    public function getTableName()
    {
        return 'CategoryLang';
    }    
        
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link,
            'LangID' => Request::getLangID()
        ], 1);
        
        return reset($res);
    }   
    
}