<?php

require_once 'Database.php';

class EventSpec extends Database
{
    
    public function getTableName()
    {
        return 'EventSpec';
    }
    
    public function getByProductID($productID)
    {
        $res = $this->getAll("WHERE `EventID` = $productID AND `LangID` = " . Request::getLangID() . " ORDER BY `Tindex`, `Rindex`");
        
        $event_specs = [];
        foreach ($res as $row)
        {
            $event_specs[$row['Tindex']][$row['Rindex']] = $row;
        }
        
        return $event_specs;
    }
    
}