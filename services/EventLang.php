<?php

require_once 'Database.php';

class EventLang extends Database
{
    
    public function getTableName()
    {
        return 'EventLang';
    }    
        
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link
        ], 1);
        
        return reset($res);
    }   
    
}