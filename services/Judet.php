<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';

class Judet
{
    
    public static $list = [
        '' => ' - ',
        'Iasi' => 'Iasi',
        'Brasov' => 'Brasov'
    ];

    public static function dropdown($selected = '')
    {
        return Html::form_dropdown('Judet', self::$list, $selected, 'class="form-control"');
    }
    
}