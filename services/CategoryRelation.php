<?php

require_once 'Database.php';

class CategoryRelation extends Database
{
    
    public function getTableName()
    {
        return 'CategoryRelation';
    }
    
    public function getRelatedCategoriesIDs($categoryID)
    {
        $categoryID = (int)$categoryID;
        
        $res = $this->query("select * from `CategoryRelation` where `Category1` = $categoryID or `Category2` = $categoryID");
        
        $ids = [];
        foreach ($res as $row)
        {
            $ids[$row['Category1']] = $row['Category1'];
            $ids[$row['Category2']] = $row['Category2'];
        }
        
        if (isset($ids[$categoryID]))
        {
            unset($ids[$categoryID]);
        }
        
        return $ids;
    }
    
    public function deleteRelations($categoryID)
    {
        $categoryID = (int)$categoryID;
        
        return $this->query("delete from `CategoryRelation` where `Category1` = $categoryID or `Category2` = $categoryID");
    }
    
    public function getRelatedCategories($categoryID)
    {
        $categoryID = (int)$categoryID;
        
        $related_ids = $this->getRelatedCategoriesIDs($categoryID);
        
        if (empty($related_ids))
        {
            return [];
        }
        else
        {
            return $this->query("
                select * from Category as c 
                left join CategoryImage as ci on ci.CategoryID = c.ID and ci.Main = 1 
                where ci.ID is not null and c.ID in (" . implode(',', $related_ids) . ")
            ");
        }
    }
    
}