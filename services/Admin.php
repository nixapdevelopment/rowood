<?php

require_once 'Database.php';

class Admin extends Database
{
    
    public function getTableName()
    {
        return 'Admin';
    }
    
    public function getForLogin($user_name, $password)
    {
        $res = $this->getWhere([
            'UserName' => $user_name,
            'Password' => $password
        ], 1);
        
        return reset($res);
    }
    
}