<?php

require_once 'Database.php';

class Page extends Database
{
    
    public function getTableName()
    {
        return 'Page';
    }
    
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link,
            'LangID' => Request::getLangID()
        ], 1);
        
        return reset($res);
    }
    
    public function getAll($addon = '')
    {
        return $this->query("SELECT * FROM " . $this->getTableName() . " AS p LEFT JOIN PageLang AS pl ON pl.PageID = p.ID AND pl.LangID = " . Request::getLangID() . " $addon");
    }
    
}