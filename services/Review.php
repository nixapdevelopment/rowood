<?php
require_once 'Database.php';

class Review extends Database
{
    
    public function getTableName()
    {
        return 'Review';
    }
    
    public function getByLink($link)
    {
        $res = $this->getWhere([
            'Link' => $link
        ], 1);
        
        return reset($res);
    }
    
    
    
    public function getAll($addon = '')
    {
        return $this->query("select * FROM Review as r left join ReviewLang as rl on rl.ReviewID = r.ID and rl.LangID = " . Request::getLangID() . " $addon");
    }
    
    public function LastinsertID() {
        return mysqli_insert_id(parent::$connection);
    }
    
}
