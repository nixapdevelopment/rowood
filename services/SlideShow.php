<?php

require_once 'Database.php';

class SlideShow extends Database
{
    
    public function getTableName()
    {
        return 'SlideShow';
    } 
    
        
    public function getAll($addon = '')
    {
        return $this->query("SELECT * FROM `"  . $this->getTableName() . "` " . $addon . " ORDER BY Sort ASC");
    }
    
}