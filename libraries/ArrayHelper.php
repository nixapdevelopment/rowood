<?php

class ArrayHelper
{
    
    public static function keyVal($array, $key_field, $value_field)
    {
        $return = [];
        
        if (count($array) > 0)
        {
            foreach ($array as $row)
            {
                $return[$row[$key_field]] = $row[$value_field];
            }
        }
        
        return $return;
    }
    
    
}