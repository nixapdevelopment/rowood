<?php


class Validator
{
    
    const ValidateEmpty = 'is_empty';
    const ValidateEmail = 'is_email';
    const ValidateNumber = 'is_number';

    
    public static $messages = [
        'errors' => [],
        'success' => []
    ];

    public static function isPost()
    {
        if (isset($_POST) && count($_POST) > 0)
        {
            return true;
        }
        
        return false;
    }

    public static function is_email($str)
    {
        return filter_var($str, FILTER_VALIDATE_EMAIL);
    }
    
    public static function is_empty($var)
    {
        return empty($var);
    }
    
    public static function is_number($number)
    {
        return filter_var($number, FILTER_VALIDATE_FLOAT);
    }
    
    public static function validate_equal($postName1, $postName2, $label1, $label2)
    {
        $value1 = Request::post($postName1);
        $value2 = Request::post($postName2);
        
        if ($value1 != $value2)
        {
            static::setError($label1 . ' and ' . $label2 . ' fields is not equal');
        }
    }
    
    public static function validate($postName, $validator, $label)
    {
        $value = Request::post($postName);
        
        switch ($validator)
        {
            case self::ValidateEmpty:
                if (self::is_empty($value))
                {
                    static::setError("Completați câmpul \"$label\"");
                }
            break;
            
            case self::ValidateEmail:
                if (!self::is_email($value))
                {
                    static::setError('Adresa email nu este validă');
                }
            break;
            
            case self::ValidateNumber:
                if (!self::is_number($value))
                {
                    static::setError(Lang::t($label) . ' field accept only number');
                }
            break;
        }
        
        return $value;
    }

    public static function setError($message, $postName = false)
    {
        if ($postName)
        {
            self::$messages['errors'][$postName] = $message;
        }
        else
        {
            self::$messages['errors'][] = $message;
        }
    }
    
    public static function setSuccess($message = false)
    {
        if (!$message)
        {
            $message = Lang::t('Data has been saved');
        }
        $_SESSION['_success'][] = $message;
    }
    
    public static function showMessages($onlyText = false)
    {
        $return = '';
        
        if ($onlyText)
        {
            if (count(static::$messages['errors']) > 0)
            {
                foreach (static::$messages['errors'] as $message)
                {
                    $return .= '<div>' . $message . '</div>';
                }
            }
            else
            {
                if (!empty($_SESSION['_success']))
                {
                    foreach ($_SESSION['_success'] as $message)
                    {
                        $return .= '<div>' . $message . '</div>';
                    }
                }
            }
            
            return $return;
        }
        
        if (count(static::$messages['errors']) > 0)
        {
            if (empty(static::$messages['errors'][0])) return $return;
            
            $return = '<div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>';
            
            foreach (static::$messages['errors'] as $message)
            {
                $return .= '<div>' . $message . '</div>';
            }
            
            $return .= '</div>';
        }
        else if (isset($_SESSION['_success']) && count($_SESSION['_success']) > 0)
        {
            $return = '<div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>';
            
            foreach ($_SESSION['_success'] as $message)
            {
                $return .= '<div>' . $message . '</div>';
            }
            
            $return .= '</div>';
        }
        
        unset($_SESSION['_success']);
        
        return $return;
    }
    
    public static function showError($postName)
    {
        return '<span class="text-danger">' . static::$messages['errors'][$postName] . '</span>';
    }

    public static function hasErrors()
    {
        if (count(static::$messages['errors']) > 0)
        {
            return true;
        }
        
        return false;
    }    
}