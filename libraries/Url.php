<?php

class Url
{

    private static function base_url()
    {
        $lang_url = '';
        $langs = Config::$languages;
        foreach ($langs as $langID => $lang){
            if(Request::getLangID() == $langID){
                $lang_url = $lang['Slug'];
            }
        }
        
        return rtrim(Config::$BaseUrl, '/') . '/' .$lang_url . '/';
    }

    public static function route($url = '', $getParams = [])
    {
        $query_string = '';
        
        if (count($getParams) > 0)
        {
            $arr = [];
            foreach ($getParams as $key => $val)
            {
                if (is_array($val))
                {
                    foreach ($val as $k => $v)
                    {
                        $arr[] = $key . '[' . $k . ']=' . $v;
                    }
                }
                else
                {
                    $arr[] = $key . '=' . $val;
                }
            }

            $query_string = '?' . implode('&', $arr);
        }
        
        return static::base_url() . ltrim(str_replace('.php', '', $url), '/') . $query_string;
    }
    
    public static function link($url)
    {
        //return static::base_url() . ltrim($url, '/');
        return rtrim(Config::$BaseUrl, '/') . '/' . ltrim($url, '/');
    }

    public static function redirect($url = '', $getParams = [])
    {
        header('Location: ' . static::route($url, $getParams));
        exit;
    }
    
    public static function strToUrl($str, $delimiter = '-')
    {
        $replace = [
            'ă' => 'a',
            'î' => 'i',
            'â' => 'a',
            'ț' => 't',
            'ș' => 's',
            'Ă' => 'a',
            'Î' => 'i',
            'Â' => 'a',
            'Ț' => 't',
            'Ș' => 's',
            
            'а' => 'a',   
            'б' => 'b',   
            'в' => 'v',
            'г' => 'g',   
            'д' => 'd',   
            'е' => 'e',
            'ё' => 'e',   
            'ж' => 'j',  
            'з' => 'z',
            'и' => 'i',   
            'й' => 'y',   
            'к' => 'k',
            'л' => 'l',   
            'м' => 'm',   
            'н' => 'n',
            'о' => 'o',   
            'п' => 'p',   
            'р' => 'r',
            'с' => 's',   
            'т' => 't',   
            'у' => 'u',
            'ф' => 'f',   
            'х' => 'h',   
            'ц' => 'c',
            'ч' => 'ch',  
            'ш' => 'sh',  
            'щ' => 'sch',
            'ь' => '\'',  
            'ы' => 'y',   
            'ъ' => 'i',
            'э' => 'e',   
            'ю' => 'yu',  
            'я' => 'ya',
        
            'А' => 'A',   
            'Б' => 'B',   
            'В' => 'V',
            'Г' => 'G',   
            'Д' => 'D',   
            'Е' => 'E',
            'Ё' => 'E',   
            'Ж' => 'J',  
            'З' => 'Z',
            'И' => 'I',   
            'Й' => 'Y',   
            'К' => 'K',
            'Л' => 'L',   
            'М' => 'M',   
            'Н' => 'N',
            'О' => 'O',   
            'П' => 'P',   
            'Р' => 'R',
            'С' => 'S',   
            'Т' => 'T',   
            'У' => 'U',
            'Ф' => 'F',   
            'Х' => 'H',   
            'Ц' => 'C',
            'Ч' => 'Ch',  
            'Ш' => 'Sh',  
            'Щ' => 'Sch',
            'Ь' => '\'',  
            'Ы' => 'Y',   
            'Ъ' => 'I',
            'Э' => 'E',   
            'Ю' => 'Yu',  
            'Я' => 'Ya'
        ];
        
        if( !empty($replace) )
        {
            $str = str_replace(array_keys($replace), array_values($replace), $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
    
    public static function switchLang($langSlug)
    {
        $uri = $_SERVER['REQUEST_URI'];
        $get_url = rtrim($_GET['url'], '/');
        
        $langID = 1;// first lang by default
        $langs = Config::$languages;
        
        foreach ($langs as $lID => $lVal){
            if($lVal['Slug'] == $langSlug){
                $langID = $lID;
            }
        }
        
        $ex_uri = explode('/', $uri);
        $page_url = '';
        
        if(isset($ex_uri[2])){
            switch ($ex_uri[2]){
                case 'p': {
                    
                    require_once DOC_ROOT . '/services/PageLang.php';                    
                    $page_lang_service = new PageLang();

                    $page_data = $page_lang_service->getByLink($get_url);
                    $page_lang_data = $page_lang_service->getWhere(['PageID' => $page_data['PageID'], 'LangID' => $langID]);
                    
                    $page_url = 'p/' . $page_lang_data[0]['Link'];
                    
                    break;
                }
                case 'c': {
                    
                    require_once DOC_ROOT . '/services/CategoryLang.php';                    
                    $cat_lang_service = new CategoryLang();

                    $cat_data = $cat_lang_service->getByLink($get_url);
                    $cat_lang_data = $cat_lang_service->getWhere(['CategoryID' => $cat_data['CategoryID'], 'LangID' => $langID]);
                    
                    $page_url = 'c/' . $cat_lang_data[0]['Link'];
                    
                    break;
                }
                case 'news': {
                    
                    require_once DOC_ROOT . '/services/NewsLang.php';                    
                    $news_lang_service = new NewsLang();

                    $news_data = $news_lang_service->getByLink($get_url);
                    $news_lang_data = $news_lang_service->getWhere(['NewsID' => $news_data['NewsID'], 'LangID' => $langID]);
                    
                    $page_url = 'news/' . $news_lang_data[0]['Link'];
                    
                    break;
                }
                case 'event': {
                    
                    require_once DOC_ROOT . '/services/EventLang.php';                    
                    $event_lang_service = new EventLang();

                    $event_data = $event_lang_service->getByLink($get_url);
                    $event_lang_data = $event_lang_service->getWhere(['EventID' => $event_data['EventID'], 'LangID' => $langID]);
                    
                    $page_url = 'event/' . $event_lang_data[0]['Link'];
                    
                    break;
                }
                default:{break;}
            }
        }
        
        return rtrim(Config::$BaseUrl, '/') . '/' . ltrim('/' . $langSlug . '/' . $page_url , '/');
    }    
}