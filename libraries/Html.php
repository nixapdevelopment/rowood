<?php

class Html {

    public static function form_dropdown($data = '', $options = array(), $selected = array(), $extra = '') {
        $defaults = array();
        if (is_array($data)) {
            if (isset($data['selected'])) {
                $selected = $data['selected'];
                unset($data['selected']); 
            }
            if (isset($data['options'])) {
                $options = $data['options'];
                unset($data['options']); 
            }
        } else {
            $defaults = array('name' => $data);
        }
        is_array($selected) OR $selected = array($selected);
        is_array($options) OR $options = array($options);
		
        if (empty($selected)) {
            if (is_array($data)) {
                if (isset($data['name'], $_POST[$data['name']])) {
                    $selected = array($_POST[$data['name']]);
                }
            } elseif (isset($_POST[$data])) {
                $selected = array($_POST[$data]);
            }
        }
        $extra = static::_attributes_to_string($extra);
        $multiple = (count($selected) > 1 && stripos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';
        $form = '<select ' . rtrim(static::_parse_form_attributes($data, $defaults)) . $extra . $multiple . ">\n";
        foreach ($options as $key => $val) {
            $key = (string) $key;
            if (is_array($val)) {
                if (empty($val)) {
                    continue;
                }
                $form .= '<optgroup label="' . $key . "\">\n";
                foreach ($val as $optgroup_key => $optgroup_val) {
                    $sel = in_array($optgroup_key, $selected) ? ' selected="selected"' : '';
                    $form .= '<option value="' . static::html_escape($optgroup_key) . '"' . $sel . '>'
                            . (string) $optgroup_val . "</option>\n";
                }
                $form .= "</optgroup>\n";
            } else {
                $form .= '<option value="' . static::html_escape($key) . '"'
                        . (in_array($key, $selected) ? ' selected="selected"' : '') . '>'
                        . (string) $val . "</option>\n";
            }
        }
        return $form . "</select>\n";
    }

    public static function _attributes_to_string($attributes) {
        if (empty($attributes)) {
            return '';
        }
        if (is_object($attributes)) {
            $attributes = (array) $attributes;
        }
        if (is_array($attributes)) {
            $atts = '';
            foreach ($attributes as $key => $val) {
                $atts .= ' ' . $key . '="' . $val . '"';
            }
            return $atts;
        }
        if (is_string($attributes)) {
            return ' ' . $attributes;
        }
        return FALSE;
    }

    public static function _parse_form_attributes($attributes, $default) {
        if (is_array($attributes)) {
            foreach ($default as $key => $val) {
                if (isset($attributes[$key])) {
                    $default[$key] = $attributes[$key];
                    unset($attributes[$key]);
                }
            }
            if (count($attributes) > 0) {
                $default = array_merge($default, $attributes);
            }
        }
        $att = '';
        foreach ($default as $key => $val) {
            if ($key === 'value') {
                $val = html_escape($val);
            } elseif ($key === 'name' && !strlen($default['name'])) {
                continue;
            }
            $att .= $key . '="' . $val . '" ';
        }
        return $att;
    }
    
    public static function html_escape($string)
    {
        return htmlentities($string);
    }
    
    public static function datePicker($postName, $value)
    {
        return '<input name="' . $postName . '" type="text" value="' . date('d.m.Y', strtotime($value)) . '" class="form-control datepicker" placeholder="">';
    }
    
    public static function priceInput($postName = 'Price', $value = 0)
    {
        return '<input type="text" class="price-input form-control" name="' . $postName . '" value="' . $value . '" />';
    }

}
