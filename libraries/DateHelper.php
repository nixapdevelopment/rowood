<?php

class DateHelper
{
    
    public static function toD($date)
    {
        return date('d', strtotime($date));
    }
    
    public static function toM($date)
    {
        return date('m', strtotime($date));
    }
    public static function toY($date)
    {
        return date('Y', strtotime($date));
    }
    
    
    public static function toDMY($date)
    {
        return date('d.m.Y', strtotime($date));
    }
    
    public static function toDMYHI($date)
    {
        return date('d.m.Y H:i', strtotime($date));
    }
    
}