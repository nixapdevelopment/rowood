<?php

class Upload
{
    
    public static function file($postName, $dist, $extensions = [])
    {
        $dist = DOC_ROOT . DIRECTORY_SEPARATOR . $dist . DIRECTORY_SEPARATOR;
        
        $postName = str_replace('[]', '', $postName);
        
        if (is_array($_FILES[$postName]['name']))
        {
            if (isset($_FILES[$postName]))
            {
                for ($i = 0; $i < count($_FILES[$postName]['name']); $i++)
                {
                    $arr = explode('.', $_FILES[$postName]['name'][$i]);
                    $ext = end($arr);
                    $ext = strtolower($ext);
        
                    if (!in_array($ext, $extensions)) continue;

                    $name = md5(microtime()) . '.' . $ext;

                    if (move_uploaded_file($_FILES[$postName]['tmp_name'][$i], $dist . $name))
                    {
                        $return[] = $name;
                    }
                }
                
                return $return;
            }
            else
            {
                return false;
            }
        }
        
        $ext = explode('.', $_FILES[$postName]['name']);
        $ext = end($ext);
        $ext = strtolower($ext);

        if (!in_array($ext, $extensions))            return FALSE;
        
        $name = md5(microtime()) . '.' . $ext;
        
        if (move_uploaded_file($_FILES[$postName]['tmp_name'], $dist . $name))
        {
            return $name;
        }
        else
        {
            return false;
        }
    }
    
    public static function ajaxFiles($dist, $extensions = [])
    {
        $dist = DOC_ROOT . DIRECTORY_SEPARATOR . $dist . DIRECTORY_SEPARATOR;
        
        $return = [];
        if (count($_FILES) > 0)
        {
            foreach ($_FILES as $key => $file)
            {
                $arr = explode('.', $file['name']);
                $ext = end($arr);
                $ext = strtolower($ext);
                if (!in_array($ext, $extensions))            return FALSE;
                
                $name = md5(microtime()) . '.' . $ext;
                
                if (move_uploaded_file($file['tmp_name'], $dist . $name))
                {
                    $return[] = $name;
                }
                else
                {
                    $return[] = false;
                }
            }
        }
        
        return $return;
    }

    public static function image($dist, $thumbWidth, $imageWidth = 1200, $extensions = ['jpg', 'png'])
    {
        $files = static::ajaxFiles($dist, $extensions);
        
        if (!is_array($files))
        {
            $files[] = $files;
        }
        
        $return = [];
        foreach ($files as $file)
        {
            $originalFile = DOC_ROOT . DIRECTORY_SEPARATOR . $dist . DIRECTORY_SEPARATOR . $file;
            
            $targetFileThumb = DOC_ROOT . DIRECTORY_SEPARATOR . $dist . DIRECTORY_SEPARATOR . 'thumb_' . $file;
            $targetFile = DOC_ROOT . DIRECTORY_SEPARATOR . $dist . DIRECTORY_SEPARATOR . $file;

            $thumb = static::resize($thumbWidth, $targetFileThumb, $originalFile);
            $image = static::resize($imageWidth, $targetFile, $originalFile);

            $return[] = [
                'thumb' => 'thumb_' . $file,
                'image' => $file
            ];
        }
        return $return;
    }
    
    public static function resize($newWidth, $targetFile, $originalFile) {

        $info = getimagesize($originalFile);
        $mime = $info['mime'];

        switch ($mime) {
                case 'image/jpeg':
                        $image_create_func = 'imagecreatefromjpeg';
                        $image_save_func = 'imagejpeg';
                        $new_image_ext = 'jpg';
                        break;

                case 'image/png':
                        $image_create_func = 'imagecreatefrompng';
                        $image_save_func = 'imagepng';
                        $new_image_ext = 'png';
                        break;

                case 'image/gif':
                        $image_create_func = 'imagecreatefromgif';
                        $image_save_func = 'imagegif';
                        $new_image_ext = 'gif';
                        break;

                default: 
                        throw new Exception('Unknown image type.');
        }

        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);

        $newHeight = ($height / $width) * $newWidth;
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        if (file_exists($targetFile)) {
                unlink($targetFile);
        }
        $image_save_func($tmp, $targetFile);
        
        return $targetFile;
    }
    
}