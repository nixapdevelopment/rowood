<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/News.php';
require_once DOC_ROOT . '/services/NewsLang.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$news_service = new News();
$news_lang_service = new NewsLang();

if (empty($_GET['url']))
{
    Url::redirect();
}

$current_article = $news_lang_service->getByLink($_GET['url']);

if (empty($current_article['NewsID']))
{
    Url::redirect();
}

$news = $news_service->getByID($current_article['NewsID']);

$TITLE = $current_article['Title'];
$KEYWORDS = $current_article['Keywords'];
$DESCRIPTION = $current_article['Description'];

?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<br />
<br />
<br />
<div class="uk-clearfix section">
    <div class="uk-container uk-container-center">
        <div class="event-item">
            <div class="heading wow fadeInUp">
                <h4>
                    <span>
                        <time style="font-size: 15px;"><?= DateHelper::toDMY($news['Date']) ?></time>
                    </span>
                </h4>
                <h2 style="font-size: 32px;">
                    <?= $current_article['Title'] ?>
                </h2>
            </div>
        </div>
        <p><?= $current_article['Text'] ?></p>                          
    </div>	
</div>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>