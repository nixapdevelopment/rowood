<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Page.php';
require_once DOC_ROOT . '/services/PageLang.php';
require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$page_service = new Page();
$page_lang_service = new PageLang();
$category_service = new Category();

if (empty($_GET['url'])) {
    Url::redirect();
}

$current_article = $page_lang_service->getByLink(rtrim($_GET['url'], '/'));

if (empty($current_article['PageID'])) {
    Url::redirect();
}

//$images = $page_image_service->getWhere([
//    'PageID' => $current_article['PageID']
//]);

$TITLE = $current_article['Title'];
$KEYWORDS = $current_article['Keywords'];
$DESCRIPTION = $current_article['Description'];
?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<br />
<br />
<br />
<section class="uk-clearfix section">
    <div class="container">
        <div class="heading wow fadeInUp">
            <h4>
                <span>
                    <?= $current_article['Title'] ?>
                </span>
            </h4>
            <h2>
                <?= $current_article['Title'] ?>
            </h2>
        </div>

        <?php
        switch ($current_article['PageID']) {
            // News page with PageID = 3
            case '3': {
                    require_once DOC_ROOT . '/services/News.php';
                    $news_service = new News();

                    $news_list = $news_service->getAll('ORDER BY Date desc');
                    ?>


                    <div class="uk-grid">
                        <?php
                        if (count($news_list) > 0) {
                            foreach ($news_list as $news) {
                                ?>                        
                            <div class="uk-width-medium-1-3">
                                <div style="position: relative; margin-bottom: 70px;" class="item">
                                    <a href="<?= Url::route('news/' . $news['Link']) ?>">
                                        <img src="<?= Url::link('assets/images/p1.jpg') ?>" alt="<?= $news['Title'] ?>" />
                                        <div style="position: absolute;top: 5px;left: 5px;color: #fff;background: rgba(17, 17, 17, 0.51);padding: 6px 11px;font-weight: 600; text-align: center;">
                                            <div><?= DateHelper::toD($news['Date']) ?> / <?= DateHelper::toM($news['Date']) ?></div>
                                            <div><?= DateHelper::toY($news['Date']) ?></div>
                                        </div>
                                    </a>
                                    <h3>
                                        <?= $news['Title'] ?>
                                    </h3>
                                    <p>
                                        <?= mb_substr(strip_tags($news['Text']), 0, 100) ?>
                                    </p>
                                </div>
                            </div>
                            <?php }
                            ?>


                        </div>          

                        <?php
                    } else {
                        print "<p>Nu exista noutati.</p>";
                    }

                    break;
                }
            // Products
            case '7':
                require_once DOC_ROOT . '/products.php';
                break;;
            // Events page with PageID = 5
            case '5': {
                    require_once DOC_ROOT . '/services/Event.php';
                    $events_service = new Event();

                    $events = $events_service->getAll('ORDER BY Date Desc');
                    ?>
                    <p>
                        <?= $current_article['Text'] ?>
                    </p>
                    <div class="row">
                        <?php
                        if (count($events) > 0) {
                            foreach ($events as $event) {
                                ?>

                                <div class="col-md-6">
                                    <a class="event-item" href="<?= Url::route('event/' . $event['Link']) ?>" title="<?= $event['Title'] ?>">
                                        <time><strong><?= DateHelper::toD($event['Date']) ?></strong><br /><?= DateHelper::toM($event['Date']) ?><br /><?= DateHelper::toY($event['Date']) ?></time>
                                        <?php if (empty($event['Thumb'])) { ?>
                                            <img src="" alt="" />
                                        <?php } else { ?>
                                            <div class="event-thumb"><img src="<?= Url::link('uploads/' . $event['Thumb']); ?>" alt="" /></div>
                                        <?php } ?>
                                        <div class="event-item-short">
                                            <strong><?= mb_substr(strip_tags($event['Title']), 0, 40) ?></strong>
                                            <p><?= mb_substr(strip_tags($event['Text']), 0, 50) ?></p>
                                        </div>
                                    </a>
                                </div>


                            <?php } ?>
                        </div>     
                        <?php
                    } else {
                        print "<p>Nu exista evenimente.</p>";
                    }

                    break;
                }
            // Donate page with PageID = 5
            case '6': {
                    ?>

                    <form action="<?= Url::route('payment') ?>.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label id="name-label" class="control-label"><?= Lang::t('Name') ?></label>
                                    <input type="text" name="Fullname" required class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Lang::t('Email') ?></label>
                                    <input type="text" name="Email" required class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Lang::t('Phone') ?></label>
                                    <input type="text" name="Phone" required class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= Lang::t('Judet') ?></label>
                                    <select required name="Judet" id="dJudet" class="form-control">
                                        <option>- selectează județ  -</option>
                                        <option>Alba</option>
                                        <option>Arad</option>
                                        <option>Arges</option>
                                        <option>Bacau</option>
                                        <option>Bihor</option>
                                        <option>Bistrita-Nasaud</option>
                                        <option>Botosani</option>
                                        <option>Brasov</option>
                                        <option>Braila</option>
                                        <option>Buzau</option>
                                        <option>Caras-Severin</option>
                                        <option>Cluj</option>
                                        <option>Constanta</option>
                                        <option>Covasna</option>
                                        <option>Dambovita</option>
                                        <option>Dolj</option>
                                        <option>Galati</option>
                                        <option>Gorj</option>
                                        <option>Harghita</option>
                                        <option>Hunedoara</option>
                                        <option>Calarasi</option>
                                        <option>Ialomita</option>
                                        <option>Iasi</option>
                                        <option>Giurgiu</option>
                                        <option>Ilfov</option>
                                        <option>Maramures</option>
                                        <option>Mehedinti</option>
                                        <option>Mures</option>
                                        <option>Neamt</option>
                                        <option>Olt</option>
                                        <option>Prahova</option>
                                        <option>Satu Mare</option>
                                        <option>Salaj</option>
                                        <option>Sibiu</option>
                                        <option>Suceava</option>
                                        <option>Teleorman</option>
                                        <option>Timis</option>
                                        <option>Tulcea</option>
                                        <option>Vaslui</option>
                                        <option>Valcea</option>
                                        <option>Vrancea</option>
                                        <option>Bucuresti</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Lang::t('Location') ?></label>
                                    <input type="text" name="Localitate" required class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label btn-block"><?= Lang::t('PaymentType') ?></label>
                                    <label class="fake-radio selected">
                                        Card
                                        <input checked type="radio" name="PaymentType" value="Card" />
                                    </label>
                                    <label class="fake-radio">
                                        PayPal
                                        <input type="radio" name="PaymentType" value="PayPal" />
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="dDonateSum">Donez (RON)</label>
                                    <input required name="DonateSum" value="" type="number" class="form-control" id="dDonateSum" placeholder="Indica suma">
                                </div>
                                <input type="hidden" name="OrderType" value="Donate" />
                                <div class="form-group">                                    
                                    <button type="submit" class="btn btn-success">Donez!</button>                                    
                                </div>
                            </div>
                        </div>
                    </form>

                    <?php
                    break;
                }
                case '8': {
                        require_once DOC_ROOT . '/calculator.php';
                }
            default : {
                    print "<p>" . $current_article['Text'] . "</p>";
                }
        }
        ?>

    </div>	
</section>



<?php require_once DOC_ROOT . '/template/footer.php'; ?>