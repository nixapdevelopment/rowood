<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Payment.php';

$payment_service = new Payment();

if (empty($_GET['orderId']))
{
    Url::redirect();
}

$order = $payment_service->getByID($_GET['orderId']);

if (empty($order['ID']))
{
    Url::redirect();
}

if (isset($_SESSION['PaypalOrderID']))
{
    $payment_service->update(['Status' => 'Paid'], ['ID' => $_SESSION['PaypalOrderID']]);
    unset($_SESSION['PaypalOrderID']);
}

$donate_order = isset($_SESSION['is_donate']);
unset($_SESSION['is_donate']);

?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<section class="content" style="margin-top: 150px;">
    <?php if ($donate_order) { ?>
    <div class="center-title">Stimate <?= $order['FirstName'] ?> <?= $order['LastName'] ?>, multumim pentru donarea</div>
    <?php } else { ?>
    <div class="center-title"><?= Lang::t('AtiRezervatCuSucces') ?></div>
    <div class="container text-center">
        Stimate <?= $order['FirstName'] ?> <?= $order['LastName'] ?>, faceți click pe butonul factură pentru a o descărca:
        <p>
            <a class="big-btn" target="_blank" href="/reports/report.php?order=<?= $order['ID'] ?>">FACTURA #<?= $order['ID'] ?></a>
        </p>
    </div>
    <?php } ?>
</section>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>