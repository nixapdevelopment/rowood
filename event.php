<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Event.php';
require_once DOC_ROOT . '/services/EventImage.php';
require_once DOC_ROOT . '/services/EventLang.php';
require_once DOC_ROOT . '/services/Judet.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';
require_once DOC_ROOT . '/services/Payment.php';
require_once DOC_ROOT . '/services/PaymentEvent.php';
require_once DOC_ROOT . '/payment/Mobilpay.php';
require_once DOC_ROOT . '/libraries/Email.php';
require_once DOC_ROOT . '/services/News.php';
require_once DOC_ROOT . '/services/NewsLang.php';
require_once DOC_ROOT . '/services/NewsRelation.php';

$news_service = new News();
$news_lang_service = new NewsLang();
$news_relations_service = new NewsRelation();

$event_service = new Event();
$event_lang_service = new EventLang();

if (empty($_GET['url'])) {
    Url::redirect();
}

$event_image_service = new EventImage();
$current_article = $event_lang_service->getByLink($_GET['url']);

if (empty($current_article['EventID'])) {
    Url::redirect();
}

$news = $news_relations_service->getRelatedNews($current_article['EventID']);

$images = $event_image_service->getWhere(['EventID' => $current_article['EventID']]);

$event = $event_service->getByID($current_article['EventID']);

if (Request::isPost()) {
    $Name = Validator::validate('Name', Validator::ValidateEmpty, 'Name');
    $Email = Validator::validate('Email', Validator::ValidateEmail, 'Email');
    $Phone = Validator::validate('Phone', Validator::ValidateEmpty, 'Phone');
    $Judet = Validator::validate('Judet', Validator::ValidateEmpty, 'Judet');
    $Location = Validator::validate('Location', Validator::ValidateEmpty, 'Location');
    $PersonType = Validator::validate('PersonType', Validator::ValidateEmpty, 'PersonType');
    $Type = Validator::validate('Type', Validator::ValidateEmpty, 'Type');
    $NrPersons = (int) Validator::validate('NrPersons', Validator::ValidateNumber, 'NrPersons');
    $PaymentType = Validator::validate('PaymentType', Validator::ValidateEmpty, 'PaymentType');
    $UseGuide = Request::post('UseGuide') == 'yes' ? 1 : 0;
    $Cui = Validator::validate('Cui', Validator::ValidateEmpty, 'CUI / CIF');
    $NrReg = Validator::validate('NrReg', Validator::ValidateEmpty, 'Nr. înregistrare la Registrul de Comerț');
    $ContBank = Request::post('ContBank');
    $Bank = Request::post('Bank');

    if (!Validator::hasErrors()) {
        $payment_service = new Payment();
        $payment_event_service = new PaymentEvent();

        $Fullname = trim($Name);
        $arr = explode(' ', $Fullname);
        $firstName = reset($arr);
        $lastName = end($arr);
        $address = $Location . ' (' . $Judet . ')';

        $amount = $event['Price'] * $NrPersons;

        if ($UseGuide) {
            $amount += $event['GuidePrice'];
        }


        $orderID = $payment_service->insert([
            'Type' => 'Event',
            'Amount' => $amount,
            'FirstName' => $firstName,
            'LastName' => $lastName,
            'Email' => $Email,
            'Phone' => $Phone,
            'Localitate' => $Location,
            'Judet' => $Judet,
            'PersonType' => $PersonType,
            'TypePriv' => $Type,
            'Cui' => $Cui,
            'NrReg' => $NrReg,
            'ContBank' => $ContBank,
            'Bank' => $Bank,
            'Status' => Payment::StatusPending
        ]);

        $payment_event_service->insert([
            'PaymentID' => $orderID,
            'EventID' => $event['ID'],
            'Tickets' => $NrPersons,
            'UseGuide' => $UseGuide
        ]);

        $event_service->query("update Event set `TicketOrdered` = `TicketOrdered` + $NrPersons where `ID` = " . $event['ID'] . " limit 1");

        $message = "$firstName $lastName a făcut o comanda în summa $amount euro.<br><br>Contacte persoanei: $phone / $email<br><br>Detalii comanda <a href=\"" . Url::route() . "reports/report.php?order=$orderID\"></a>";

        Email::send(Config::$DonateEmail, 'Comanda la ' . Url::route(), $message);

        if ($PaymentType == 'Card')
        {
            Mobilpay::process($orderID, $amount, $returnUrl, $firstName, $lastName, $address, $Email, $Phone);
            exit;
        }
        elseif ($PaymentType == 'PayPal')
        {
            $_SESSION['PaypalOrderID'] = $orderID;
        ?>
        <form id="paypal-form" action="https://www.sandbox.paypal.com/cgi-bin/websc" method="post">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="colesnic89@gmail.com">
            <input id="paypalItemName" type="hidden" name="item_name" value="Inscriere <?= $NrPersons ?> pers. la eveniment: <?= $current_article['Title'] ?>">
            <input id="paypalQuantity" type="hidden" name="quantity" value="1">
            <input id="paypalAmmount" type="hidden" name="amount" value="<?php echo $amount / 4.5; ?>">
            <input type="hidden" name="no_shipping" value="1">
            <input type="hidden" name="return" value="<?php echo Url::route('payment_confirm', ['orderId' => $orderID]); ?>">

            <input type="hidden" name="custom" value="{}">

            <input type="hidden" name="currency_code" value="EUR">
            <input type="hidden" name="lc" value="RO">
            <input type="hidden" name="bn" value="PP-BuyNowBF">
        </form>
        <script>document.getElementById("paypal-form").submit();</script>
        <?php
        exit;
        }
        else
        {
            Url::redirect('payment_confirm', ['orderId' => $orderID]);
        }
    }
}


$TITLE = $current_article['Title'];
$KEYWORDS = $current_article['Keywords'];
$DESCRIPTION = $current_article['Description'];
?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>



<section class="content" style="margin-top: 150px;">
    <div class="container">
        <div class="event-item" href="<?= Url::route('event/' . $event['Link']) ?>" title="<?= $event['Title'] ?>">
            <time><strong><?= DateHelper::toD($event['Date']) ?></strong><br /><?= DateHelper::toM($event['Date']) ?><br /><?= DateHelper::toY($event['Date']) ?></time>

            <div class="event-item-short">
                <h3><strong><?= $current_article['Title'] ?></strong></h3>
            </div>
        </div>
        <p><?= $current_article['Text'] ?></p>
        <hr />
        <div class="page-images">
            <div class="row">
                <?php foreach ($images as $image) { ?>
                    <div class="col-md-2">
                        <a rel="page-gallery" class="lightbox page-thumb" href="<?= Url::link('uploads/' . $image['Image']); ?>"><img src="<?= Url::link('uploads/' . $image['Thumb']); ?>" /></a>
                    </div>
                <?php } ?>
            </div>


        </div>
        <hr />

        <h4><small>Articole legate de evenimentul:</small> <strong><?= $current_article['Title'] ?></strong></h4>
        <?php foreach ($news as $new) { ?>
            <a href=""><?= mb_substr(strip_tags($new['Title']), 0, 56) ?> ...</a>, 
        <?php } ?>

        <hr />

        <button data-toggle="collapse" data-target="#order-form" aria-expanded="false" class="big-btn"><?= Lang::t('rez_title') ?></button></h3>

        <div id="order-form" class="panel panel-default collapse">
            <div class="panel-heading">
                <h4><?= Lang::t('buy_ticket') ?></h4>
            </div>
            <div class="panel-body">
                <form method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label btn-block"><?= Lang::t('PersonType') ?></label>
                                <label class="fake-radio selected">
                                    Persoana fizica
                                    <input type="radio" onclick="Register.setFormPF()" checked name="PersonType" value="PF" />
                                </label>
                                <label class="fake-radio">
                                    Persoana juridica
                                    <input type="radio" onclick="Register.setFormPJ()" name="PersonType" value="PJ" />
                                </label>
                            </div>
                            <div class="form-group">
                                <label id="name-label" class="control-label"><?= Lang::t('Name') ?></label>
                                <input type="text" name="Name" required class="form-control" />
                            </div>
                            <div id="cui" class="form-group">
                                <label class="control-label"><?= Lang::t('CuiCif') ?></label>
                                <input type="text" name="Cui" class="form-control" />
                            </div>
                            <div id="reg_number" class="form-group">
                                <label class="control-label"><?= Lang::t('NrReg') ?></label>
                                <input type="text" name="NrReg" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= Lang::t('Email') ?></label>
                                <input type="text" name="Email" required class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= Lang::t('Phone') ?></label>
                                <input type="text" name="Phone" required class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= Lang::t('Judet') ?></label>
                                <select name="Judet" class="form-control">
                                    <option>- selectează județ  -</option>
                                    <option>Alba</option>
                                    <option>Arad</option>
                                    <option>Arges</option>
                                    <option>Bacau</option>
                                    <option>Bihor</option>
                                    <option>Bistrita-Nasaud</option>
                                    <option>Botosani</option>
                                    <option>Brasov</option>
                                    <option>Braila</option>
                                    <option>Buzau</option>
                                    <option>Caras-Severin</option>
                                    <option>Cluj</option>
                                    <option>Constanta</option>
                                    <option>Covasna</option>
                                    <option>Dambovita</option>
                                    <option>Dolj</option>
                                    <option>Galati</option>
                                    <option>Gorj</option>
                                    <option>Harghita</option>
                                    <option>Hunedoara</option>
                                    <option>Calarasi</option>
                                    <option>Ialomita</option>
                                    <option>Iasi</option>
                                    <option>Giurgiu</option>
                                    <option>Ilfov</option>
                                    <option>Maramures</option>
                                    <option>Mehedinti</option>
                                    <option>Mures</option>
                                    <option>Neamt</option>
                                    <option>Olt</option>
                                    <option>Prahova</option>
                                    <option>Satu Mare</option>
                                    <option>Salaj</option>
                                    <option>Sibiu</option>
                                    <option>Suceava</option>
                                    <option>Teleorman</option>
                                    <option>Timis</option>
                                    <option>Tulcea</option>
                                    <option>Vaslui</option>
                                    <option>Valcea</option>
                                    <option>Vrancea</option>
                                    <option>Bucuresti</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= Lang::t('Location') ?></label>
                                <input type="text" name="Location" required class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="cont_bancar" class="form-group">
                                <label class="control-label"><?= Lang::t('ContBancar') ?></label>
                                <input type="text" name="ContBank" class="form-control" />
                            </div>
                            <div id="bank" class="form-group">
                                <label class="control-label"><?= Lang::t('Bank') ?></label>
                                <select class="form-control" name="Bank">
                                    <option value="" selected="selected">- selectează -</option>
                                    <option>Alpha Bank</option>
                                    <option>Anglo-Romanian Bank</option>
                                    <option>ATE Bank</option>
                                    <option>Banca C.R. Firenze</option>
                                    <option>Banca Comerciala Carpatica</option>
                                    <option>EximBank</option>
                                    <option>Banca di Roma</option>
                                    <option>Banca Italo Romena</option>
                                    <option>Banca Romaneasca</option>
                                    <option>Banca Transilvania</option>
                                    <option>Bancpost</option>
                                    <option>BCR</option>
                                    <option>BRD - Groupe Societe Generale</option>
                                    <option>CEC</option>
                                    <option>Citibank</option>
                                    <option>CreditEurope Bank</option>
                                    <option>Egnatia Bank</option>
                                    <option>Emporiki Bank</option>
                                    <option>GarantiBank</option>
                                    <option>HVB Locuinte</option>
                                    <option>ING Bank</option>
                                    <option>la Caixa</option>
                                    <option>Leumi Bank</option>
                                    <option>Libra Bank</option>
                                    <option>Marfin Bank</option>
                                    <option>Millennium Bank</option>
                                    <option>MKB Romexterra Bank</option>
                                    <option>OTP Bank</option>
                                    <option>Piraeus Bank</option>
                                    <option>Porsche Bank</option>
                                    <option>ProCredit Bank</option>
                                    <option>Raiffeisen Bank</option>
                                    <option>Raiffeisen Locuinte</option>
                                    <option>RBS</option>
                                    <option>Romanian International Bank</option>
                                    <option>Sanpaolo Imi Bank</option>
                                    <option>UniCredit Tiriac</option>
                                    <option>Volksbank</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label btn-block"><?= Lang::t('PaymentType') ?></label>
                                <label class="fake-radio selected">
                                    <?= Lang::t('TransferBancar') ?>
                                    <input type="radio" checked name="PaymentType" value="Bank" />
                                </label>
                                <label class="fake-radio">
                                    Card
                                    <input type="radio" name="PaymentType" value="Card" />
                                </label>
                                <label class="fake-radio">
                                    PayPal
                                    <input type="radio" name="PaymentType" value="PayPal" />
                                </label>
                            </div>
                            <div id="tva" class="form-group">
                                <label class="control-label"><?= Lang::t('Tva') ?></label>
                                <br />
                                <label class="fake-radio selected">
                                    Da
                                    <input type="radio"  name="Tva" value="Da" />
                                </label>
                                <label class="fake-radio">
                                    Nu
                                    <input type="radio" checked name="Tva" value="Nu" />
                                </label>                                   
                            </div>
                            <div id="tip" class="form-group">
                                <label class="control-label"><?= Lang::t('Tip') ?></label>
                                <select class="form-control" name="Type">
                                    <option value="1">Student</option>
                                    <option value="2">Elev</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= Lang::t('NrPersons') ?></label>
                                <input onchange="calcTotal()" onkeyup="calcTotal()" step="1" min="1" value="1" type="number" name="NrPersons" required class="form-control" />
                            </div>  
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label btn-block"><?= Lang::t('Ghidaj') ?></label>
                                        <label class="fake-radio selected">
                                            Nu
                                            <input onchange="calcTotal()" type="radio" checked name="UseGuide" value="no" />
                                        </label>
                                        <label class="fake-radio">
                                            Da
                                            <input onchange="calcTotal()" type="radio" name="UseGuide" value="yes" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pull-left">
                                <label class="control-label"><?= Lang::t('Cost') ?></label>
                                <input style="font-weight: bold;" type="number" readonly name="Price" class="form-control" value="<?= $event['Price'] ?>" />
                            </div>
                            <div class="form-group form-group-lg pull-right">
                                <label class="control-label">&nbsp;</label>
                                <button type="submit" class="btn btn-danger btn-block"><i class="glyphicon glyphicon-shopping-cart"></i> &nbsp;<?= Lang::t('order') ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>	
</section>
<br />
<br />

<script>

    function calcTotal()
    {
        var total = <?= $event['Price'] ?> * $('input[name="NrPersons"]').val();

        if ($('input[name="UseGuide"]:checked').val() == 'yes')
        {
            total += <?= $event['GuidePrice'] ?>;
        }

        $('input[name="Price"]').val(total.toFixed(2));
    }

    var Register = {
        setFormPF: function ()
        {
            $('#cui, #reg_number, #bank, #cont_bancar').hide().find('input').removeAttr('required');
            $('#name-label').text('Nume');
            $('#tva').hide();
            $('#tip').show();
        },
        setFormPJ: function ()
        {
            $('#cui, #reg_number, #bank, #cont_bancar').show();
            $('#cui input').attr('required', 'required');
            $('#name-label').text('Nume companie');
            $('#tva').show();
            $('#tip').hide();
        }
    }
</script>


<?php require_once DOC_ROOT . '/template/footer.php'; ?>