<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/Feedback.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$feedback_service = new Feedback();

$feedbacks = $feedback_service->getAll('order by Date desc');

if (isset($_GET['delID']))
{
    $ID = (int)$_GET['delID'];
    
    $feedback_service->deleteByID($ID);
    
    Validator::setSuccess('Feedback a fost eliminat');
    
    Url::redirect('admin/feedback/index.php');
}

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Feedback</h1>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="text-center">№</th>
                    <th>Nume</th>
                    <th>Prenume</th>
                    <th>Email</th>
                    <th>Telefon</th>
                    <th>Mesaj</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($feedbacks) > 0) { ?>
                <?php $i = 0; ?>
                <?php foreach ($feedbacks as $article) { ?>
                <?php $i++; ?>
                <tr>
                    <td class="text-center"><?= $i ?></td>
                    <td><?= $article['FirstName'] ?></td>
                    <td><?= $article['LastName'] ?></td>
                    <td><?= $article['Email'] ?></td>
                    <td><?= $article['Phone'] ?></td>
                    <td>
                        <span class="btn btn-link" data-placement="bottom" data-toggle="popover" title="Mesaj" data-content="<?= htmlentities(strip_tags($article['Message'])) ?>">Vezi mesaj</span>
                    </td>
                    <td>
                        <a href="<?= Url::route('admin/feedback/index.php', ['delID' => $article['ID']]) ?>" onclick="return confirm('Confirmați?')" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                    </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                    <td colspan="7" class="text-center">
                        Nu este nici o mesaj
                    </td> 
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>

    $(function () {
        
        $('[data-toggle="popover"]').popover();

        $(document).on('click', function (e) {
            $('[data-toggle="popover"],[data-original-title]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
                    (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false
                }
            });
        });
      
    });

</script>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>