<?php
require_once DOC_ROOT . '/core/core.php';

if (empty($_SESSION['IsAdmin']))
{
    Url::redirect('admin/login.php');
}