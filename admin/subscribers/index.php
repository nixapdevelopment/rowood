<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/Subscribe.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$subscribe_service = new Subscribe();

$subscribers = $subscribe_service->getAll('order by Date desc');

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Abonați</h1>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <div class="row">
            <div class="col-md-5">
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th class="text-center">№</th>
                            <th>Email</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($subscribers) > 0) { ?>
                        <?php $i = 0; ?>
                        <?php foreach ($subscribers as $article) { ?>
                        <?php $i++; ?>
                        <tr>
                            <td class="text-center"><?= $i ?></td>
                            <td><?= $article['Email'] ?></td>
                            <td><?= DateHelper::toDMY($article['Date']) ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                            <td colspan="7" class="text-center">
                                Nu este nici o abonat
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        
        $('[data-toggle="popover"]').popover();

        $(document).on('click', function (e) {
            $('[data-toggle="popover"],[data-original-title]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
                    (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false
                }
            });
        });
      
    });

</script>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>