<?php
require '../../includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/SlideShow.php';

$slideshow_service = new SlideShow();

$slides = [];
$slides['left'] = $slideshow_service->getAll("WHERE Position = 'left'");
$slides['right'] = $slideshow_service->getAll("WHERE Position = 'right'");

if (Request::isPost())
{
    
    if (!Validator::hasErrors())
    {
//        print "<pre>";
//        print_r($_FILES);
//        exit();
        
        
        if (!empty($_FILES['Photo_left']['name']))
        {            
            $uploaded_files = Upload::file('Photo_left', 'uploads/slideshow', ['png', 'jpg']);
                    
            if (!empty($uploaded_files))
            {     
                $x100 = Upload::resize(800, DOC_ROOT . '/uploads/slideshow/800px' . $uploaded_files, DOC_ROOT . '/uploads/slideshow/' . $uploaded_files);
                $x100 = explode('/', $x100);
                $x100 = end($x100);
                    
                $data = [
                    'Url' => $uploaded_files,
                    'Thumb_url' => $x100,
                    'Type' => 'foto',
                    'Position' => 'left'
                ];                
                
                $slideshow_service->insert($data);
            }
        }else if(!empty($_POST['Video_left'])) {
                        
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $_POST['Video_left'], $matches);
            $data = [
                    'Url' => $matches[1],
                    'Type' => 'video',
                    'Position' => 'left'
                ];  
            
            $slideshow_service->insert($data);
        }
        
        if (!empty($_FILES['Photo_right']['name']))
        {
            $uploaded_files = Upload::file('Photo_right', 'uploads/slideshow', ['png', 'jpg']);
                    
            if (!empty($uploaded_files))
            {     
                $x100 = Upload::resize(800, DOC_ROOT . '/uploads/slideshow/800px' . $uploaded_files, DOC_ROOT . '/uploads/slideshow/' . $uploaded_files);
                $x100 = explode('/', $x100);
                $x100 = end($x100);
                    
                $data = [
                    'Url' => $uploaded_files,
                    'Thumb_url' => $x100,
                    'Type' => 'foto',
                    'Position' => 'right'
                ];                
                
                $slideshow_service->insert($data);
            }
        }else if(!empty($_POST['Video_right'])){
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $_POST['Video_right'], $matches);
            $data = [
                    'Url' => $matches[1],
                    'Type' => 'video',
                    'Position' => 'right'
                ];  
            
            $slideshow_service->insert($data);
        }
        
        Validator::setSuccess('Informatia a fost salvata!');
        Url::redirect('admin/slideshow/index.php');
    }
}

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">
        <div>
            <?= Validator::showMessages() ?>
        </div>
        
        <form method="post" enctype="multipart/form-data">
                
        <div class="row">            
            
            <h1>Slide Show</h1>                 
            <div class="col-md-6">
               <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#foto-left-col">Foto</button>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-info pull-right" data-toggle="collapse" data-target="#video-left-col">Video</button>
                    </div>                          
               </div>
               <br />
               <div class="row">                   
                    <div id="foto-left-col" class="collapse">  
                         <div class="input-group">
                              <input name="Photo_left" type="file" class="form-control" id="fcPhotos" placeholder="Photos" /> 
                              <span class="input-group-btn">
                                   <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                              </span>
                         </div>
                    </div>           

                    <div>                            
                         <div id="video-left-col" class="collapse">
                             <div class="form-group">
                                 <input onkeyup="validateYouTubeUrl(this)" type="text" name="Video_left" class="form-control" value="" placeholder="i.e. http://youtu.be/example" />
                             </div>
                         </div>
                     </div>                    
               </div>

                <br /><br />
                
                <div class="row">

                    <ul id="slide_show_list_left" class="list-group">

                    <?php                      
                    if (count($slides['left']) > 0) {
                       foreach ($slides['left'] as $slideL) { ?>
                           <li class="ui-state-default list-group-item" data-item="<?= $slideL['ID']; ?>">
                               <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                               <?php if ($slideL['Type'] == 'foto') { ?>
                               <a href="<?= Url::link('uploads/slideshow/' . $slideL['Url']) ?>" class="fancybox">
                                   <img class="img-thumbnail" style="height: 35px;" src="<?= Url::link('uploads/slideshow/' . $slideL['Thumb_url']) ?>" />
                               </a>
                               <?php } else { ?>
                               <i style="font-size: 30px;" class="glyphicon glyphicon-film"></i> <a style="color: #0066cc;;" target="_blank" href="https://www.youtube.com/watch?v=<?= $slideL['Url'] ?>">View</a>
                               <?php } ?>
                               <button type="button" class="btn btn-danger pull-right" onclick="removeSlideShow('<?= $slideL['ID']; ?>');"><i class="glyphicon glyphicon-trash"></i></button>
                               <div class="clearfix"></div>
                           </li>      
                    <?php
                       }
                    }
                    ?>

                    </ul>
                    
                </div>

                <br /><br />  
                
            </div>
            
        </div> 
        <div class="row">
            <div>
                <button class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Salvati</button>
            </div>
        </div>
        
    </form>
        
    </div>
</div>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>

<script>
    
    function removeSlideShow(id){
        if(confirm("Confirmati?")){ 
            $.post('<?= Url::route('ajax/delete-slideshow-image.php') ?>', {imgID: id}, function(){
                $("#slide_item_"+id).remove();
            });                        
            return false;
        }
        else return false;
    }
    
    $('#foto-left-col').on('show.bs.collapse', function (){        
        $('#video-left-col').collapse('hide'); 
        $('input[name=Video_left]').val('');
    });
    
    $('#video-left-col').on('show.bs.collapse', function (){        
        $('#foto-left-col').collapse('hide');  
        $('input[name=Photo_left]').val('');
    });
    
    $('#foto-right-col').on('show.bs.collapse', function (){        
        $('#video-right-col').collapse('hide'); 
        $('input[name=Video_right]').val('');
    });
    
    $('#video-right-col').on('show.bs.collapse', function (){        
        $('#foto-right-col').collapse('hide');  
        $('input[name=Photo_right]').val('');
    });
</script>
<link href="<?= Url::link('assets/js/jquery-ui-1.12.1.custom/jquery-ui.css') ?>" rel="stylesheet">
<script src="<?= Url::link('assets/js/jquery-ui-1.12.1.custom/jquery-ui.js') ?>"></script>
<script>
$( function() {
  $( "#slide_show_list_left" ).sortable({
      update: function( event, ui ) {
        var data = $("#slide_show_list_left").sortable('toArray', {attribute: "data-item"});  
        // return ids in order after update
        //alert(JSON.stringify(data));
        $.post('<?= Url::route('ajax/sort-slideshow-image.php') ?>', {sortedIDs: JSON.stringify(data)}, function(){});
      },
      opacity: 0.7
    });
  $( "#slide_show_list_right" ).sortable({
      update: function( event, ui ) {
        var data = $("#slide_show_list_right").sortable('toArray', {attribute: "data-item"});  
        // return ids in order after update
        //alert(JSON.stringify(data));
        $.post('<?= Url::route('ajax/sort-slideshow-image.php') ?>', {sortedIDs: JSON.stringify(data)}, function(){});
      },
      opacity: 0.7
    });
  $( "#slide_show_list_left" ).disableSelection();
  $( "#slide_show_list_right" ).disableSelection();
  
  $('input[name=Video_right], input[name=Video_left]').on('input propertychange', function() {
        validateYouTubeUrl(this);
    });
  
} );

function validateYouTubeUrl(elem)
{
    var url = $(elem).val();
    if (url != undefined || url != '')
    {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11)
        {
            $(elem).closest('.form-group').removeClass('has-error').addClass('has-success');
        }
        else
        {
            $(elem).closest('.form-group').addClass('has-error');
        }
    }
}

</script>