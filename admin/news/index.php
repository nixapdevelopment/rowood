<?php
require '../../includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/News.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$news_service = new News();

$news = $news_service->getAll('order by Date desc');

if (isset($_GET['delID']))
{
    $ID = (int)$_GET['delID'];
    
    $news_service->deleteByID($ID);
    
    Validator::setSuccess('Noutatea a fost eliminată');
    
    Url::redirect('admin/news/index.php');
}

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Noutăți</h1>
        <div>
            <a href="<?= Url::route('admin/news/edit.php') ?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Adaugă</a>
        </div>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="text-center">№</th>
                    <th>Titlu</th>
                    <th>Link</th>
                    <th>Data</th>
                    <th>Text</th>
                    <th style="width: 160px;"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($news) > 0) { ?>
                <?php $i = 0; ?>
                <?php foreach ($news as $article) { ?>
                <?php $i++; ?>
                <tr>
                    <td class="text-center"><?= $i ?></td>
                    <td><?= $article['Title'] ?></td>
                    <td><a target="_blank" href="<?= Url::route('blog/' . $article['Link']) ?>"><?= $article['Link'] ?></a></td>
                    <td><?= DateHelper::toDMY($article['Date']) ?></td>
                    <td><?= mb_substr(strip_tags($article['Text']), 0, 70) ?>...</td>
                    <td style="width: 160px; text-align: center">
                        <a href="<?= Url::route('admin/news/edit.php', ['id' => $article['ID']]) ?>" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                        &nbsp;
                        <a href="<?= Url::route('admin/news/index.php', ['delID' => $article['ID']]) ?>" onclick="return confirm('Confirmați?')" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                    </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                    <td colspan="6" class="text-center">
                        Nu este nici o noutate
                    </td> 
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
</div>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>