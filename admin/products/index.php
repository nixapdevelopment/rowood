<?php
require '../../includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/Event.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$event_service = new Event();

$event = $event_service->getAll('order by e.ID desc');

if (isset($_GET['delID']))
{
    $ID = (int)$_GET['delID'];
    
    $event_service->deleteByID($ID);
    
    Validator::setSuccess('Evenimentul a fost eliminat');
    
    Url::redirect('admin/products/index.php');
}

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Produse</h1>
        <div>
            <a href="<?= Url::route('admin/products/edit.php') ?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Adaugă</a>
        </div>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="text-center">№</th>
                    <th>Titlu</th>
                    <th>Categorie</th>
                    <th>Link</th>
                    <th style="width: 160px;"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($event) > 0) { ?>
                <?php $i = 0; ?>
                <?php foreach ($event as $article) { ?>
                <?php $i++; ?>
                <tr>
                    <td class="text-center"><?= $i ?></td>
                    <td><?= $article['Title'] ?></td>
                    <td><?= $article['CategoryName'] ?></td>
                    <td><a target="_blank" href="<?= Url::route('product/' . $article['Link']) ?>"><?= $article['Link'] ?></a></td>
                    <td style="width: 160px; text-align: center">
                        <a href="<?= Url::route('admin/products/edit.php', ['id' => $article['EventID']]) ?>" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                        &nbsp;
                        <a href="<?= Url::route('admin/products/index.php', ['delID' => $article['EventID']]) ?>" onclick="return confirm('Confirmați?')" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                    </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                    <td colspan="6" class="text-center">
                        Nu exista nici un eveniment
                    </td> 
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
</div>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>