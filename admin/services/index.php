<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Service.php';

$services_service = new Service();

$services = $services_service->getAll('order by ID');

if (isset($_GET['delID']))
{
    $ID = (int)$_GET['delID'];
    
    $services_service->deleteByID($ID);
    
    Validator::setSuccess('Serviciu a fost eliminată');
    
    Url::redirect('admin/services/index.php');
}

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Servicii</h1>
        <div>
            <a href="<?= Url::route('admin/services/edit.php') ?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Adaugă</a>
        </div>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="text-center">№</th>
                    <th>Titlu</th>
                    <th>Text</th>
                    <th style="width: 160px;"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($services) > 0) { ?>
                <?php $i = 0; ?>
                <?php foreach ($services as $service) { ?>
                <?php $i++; ?>
                <tr>
                    <td class="text-center"><?= $i ?></td>
                    <td><?= $service['Title'] ?></td>
                    <td><?= mb_substr(strip_tags($service['Text']), 0, 90) ?>...</td>
                    <td style="width: 160px; text-align: center">
                        <a href="<?= Url::route('admin/services/edit.php', ['id' => $service['ID']]) ?>" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                        &nbsp;
                        <a href="<?= Url::route('admin/services/index.php', ['delID' => $service['ID']]) ?>" onclick="return confirm('Confirmați?')" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                    </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                    <td colspan="4" class="text-center">
                        Nu este nici o serviciu
                    </td> 
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>