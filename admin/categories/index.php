<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/services/CategoryLang.php';
require_once DOC_ROOT . '/services/CategoryRelation.php';

$category_service = new Category();
$category_lang_service = new CategoryLang();
$category_relation_service = new CategoryRelation();

$categories = $category_service->getTree(Request::getLangID());

if (isset($_GET['delID']))
{
    $ID = (int)$_GET['delID'];
    
    $category_service->deleteByID($ID);
    $category_lang_service->delete(['CategoryID' => $ID]);
    $category_relation_service->deleteRelations($ID);
    
    Validator::setSuccess('Categorie a fost eliminată');
    
    Url::redirect('admin/categories/index.php');
}

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Categorii <span><a href="<?= Url::route('admin/categories/edit.php') ?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Adaugă</a></span></h1>
        <div>
            
        </div>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>        
        
        <?php if(count($categories) > 0) 
                  {echo $categories;}
              else { ?>
        <p>Nu este nici o categorie</p>
        <?php } ?>         
        
    </div>
</div>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>