<?php
require '../../includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/Review.php';
require_once DOC_ROOT . '/services/ReviewLang.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';


$review_service = new Review();
$review_lang_service = new ReviewLang();

$ID = (!empty($_GET['ID'])) ? (int) $_GET['ID'] : 0;
$username = Request::post('Username');
$subtitle = Request::post('Subtitle');
$text = Request::post('Text');

if (Request::post()) {
    if (!Validator::hasErrors()) {
        $review = [
            "Sort" => 0,
            "Date" => date('Y-m-d', strtotime(Request::post('Date'))),
            'Link' => Request::post('Link')
        ];
        
        if (!empty($_FILES['Image']['tmp_name']))
        {
            $icon = Upload::image('uploads/reviews', 150, 150, ['png', 'jpg', 'jpeg']);
        }
        else
        {
            $icon = [];
        }

        if (!($ID > 0)) {
            if (count($icon) > 0)
            {
                $ic = reset($icon);
                $review['Image'] = $ic['image'];
            }
            $ID = $review_service->insert($review);
        }
        else
        {
            $review = [
                "Date" => date('Y-m-d', strtotime(Request::post('Date'))),
                'Link' => Request::post('Link')
            ];
            
            if (count($icon) > 0)
            {
                $ic = reset($icon);
                $review['Image'] = $ic['image'];
            }
            
            $review_service->update($review, ['ID' => $ID]);
        }

        $review_lang_service->delete(["ReviewID" => $ID]);
        $v_langs = [];
        foreach ($username as $langID => $ttl) {
            $v_langs[] = [
                'ReviewID' => $ID,
                'LangID' => $langID,
                'Username' => $ttl,
                'Subtitle' => $subtitle[$langID],
                'Text' => $text[$langID]
            ];
        }
        $review_lang_service->insert_batch($v_langs);

        Url::redirect('admin/reviews/index.php');
    }
}

if ($ID > 0) {
    $review_item = $review_service->getByID($ID);
    $review = $review_lang_service->getWhere(["ReviewID" => $ID]);
    
    $review_langs = [];
    foreach ($review as $rev)
    {
        $review_langs[$rev['LangID']] = $rev;
    }
}

$reviews = $review_service->getAll('ORDER BY Sort');
?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>
<div class="container">
    <div class="row">
        <?= Validator::showMessages() ?>
        <form id="AddVideoForm" method="post" enctype="multipart/form-data">
            <div class="AddVideoFormMessage"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label">Poza</label>
                                <input class="form-control" type="file" name="Image" <?= empty($review_item['Image']) ? 'required' : '' ?> accept=".png,.jpg,.jpeg" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?php if (!empty($review_item['Image'])) { ?>
                            <img src="<?= Url::link('uploads/reviews/' . $review_item['Image']) ?>" class="img-thumbnail" style="height: 80px;" />
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"> 
                        <label class="control-label" for="fcTitle">Data <span class="text-danger">*</span></label>
                        <input name="Date" required value="<?= DateHelper::toDMY(!empty($review_item['Date']) ? $review_item['Date'] : date('c')) ?>" type="text" class="form-control date-picker" id="fcLink" placeholder="Data">
                    </div>
                </div>
            </div>
            
            <div class="form-group"> 
                <label class="control-label" for="fcTitle">Link <span class="text-danger">*</span></label>
                <input name="Link" required value="<?= !empty($review_item['Link']) ? $review_item['Link'] : '' ?>" type="url" class="form-control" placeholder="Link">
            </div>

            <ul class="nav nav-tabs" role="tablist">
                <?php foreach (Config::$languages as $langID => $lang) { ?>
                    <li role="presentation" class="<?= $langID == 1 ? 'active' : '' ?>"><a href="#<?= $lang['Slug'] ?>" aria-controls="<?= $lang['Slug'] ?>" role="tab" data-toggle="tab"><?= $lang['Name'] ?></a></li>
                <?php } ?>
            </ul>

            <div class="tab-content">
                <br />
                <?php foreach (Config::$languages as $langID => $lang) { ?>
                    <div role="tabpanel" class="tab-pane <?= $langID == 1 ? 'active' : '' ?>" id="<?= $lang['Slug'] ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <label class="control-label" for="fcTitle">Titlu <span class="text-danger">*</span></label>
                                    <input name="Username[<?= $langID ?>]" required value="<?= (!empty($review_langs)) ? $review_langs[$langID]['Username'] : ''; ?>" type="text" class="form-control" id="fcLink" placeholder="Nume utilizator">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <label class="control-label">Subtitlu <span class="text-danger">*</span></label>
                                    <input name="Subtitle[<?= $langID ?>]" required value="<?= (!empty($review_langs)) ? $review_langs[$langID]['Subtitle'] : ''; ?>" type="text" class="form-control" placeholder="Subtitlu">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">   
                            <label class="control-label" for="fcTitle">Text <span class="text-danger">*</span></label>
                            <textarea class="ckeditor" name="Text[<?= $langID ?>]" class="form-control" id="fcLink" placeholder="Text utilizator"><?= (!empty($review_langs)) ? $review_langs[$langID]['Text'] : ''; ?></textarea>
                        </div>
                    </div>                 
                <?php } ?>
            </div>
            <input name="submitReview" value="Salveaza" type="submit" class="btn btn-primary" id="fcLink">
        </form>  
        <hr />
        <div class="clearfix"></div>
        <div id='SortorederVid'></div>
        <div class="panel panel-default">
            <div class="panel-heading">Review items</div>
            <div class="panel-body" id="EditProducts">
                <ul id="video-items" class="list-group">

                    <?php
                    if (!empty($reviews)) {
                        foreach ($reviews as $vid) {
                            ?>
                            <li class="list-group-item" data-review-id="<?= $vid['ID'] ?>">
                                <i style="cursor: move;" class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;&nbsp;<?= $vid['Username'] ?><i style="cursor: pointer; font-size: 18px; margin-left: 10px;" class="glyphicon glyphicon-trash text-danger pull-right"></i>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;" class="pull-right" href="<?= Url::link('admin/reviews/index.php?ID=' . $vid['ID']) ?>"><i class="glyphicon glyphicon-edit"></i></a>
                            </li>
                            <?php
                        }
                    }//if 
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>





<script>
    $(function () {


        $("#video-items").sortable({
            placeholder: "ui-state-highlight",
            update: function (evt, ui) {
                var order = $(this).sortable('toArray', {attribute: 'data-review-id'});

                $.ajax({
                    type: "POST",
                    url: "<?= Url::route('ajax/SortReviews.php') ?>",
                    data: {'ID': order},
                    success: function (msg) {
                        $("#SortorederVid").html(msg);
                    }



                });
            }
        });
        $("#sortable").disableSelection();

        $('#video-items').on('click', '.glyphicon-trash', function () {
            if (confirm("Doriti sa stergeti aceasta recenzie ?")) {
                var li = $(this);

                $.ajax({
                    type: "POST",
                    url: "<?= Url::route('ajax/DeleteReview.php') ?>",
                    data: {ReviewID: $(this).closest('li').attr('data-review-id')},
                    success: function (msg) {
                        $("#SortorederVid").html(msg);
                        li.closest('li').remove();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }



                });


            }


        });

    });

</script>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>