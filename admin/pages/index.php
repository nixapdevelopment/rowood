<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/Page.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$page_service = new Page();

$pages = $page_service->getAll('order by `Position`');

if (isset($_GET['delID']))
{
    $ID = (int)$_GET['delID'];    
       
    foreach ($pages as $pageID => $page){ 
        
        if($ID == $page['ID'] && $page['System'] != 1){            
            
            $page_service->deleteByID($ID);
    
            Validator::setSuccess('Pagina a fost eliminată');
            
        }
    } 
    
    Url::redirect('admin/pages/index.php');
    
}

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Pagini</h1>
        <div>
            <a href="<?= Url::route('admin/pages/edit.php') ?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Adaugă</a>
        </div>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="text-center">№</th>
                    <th>Titlu</th>
                    <th>Link</th>
                    <th>Text</th>
                    <th style="width: 160px;"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($pages) > 0) { ?>
                <?php $i = 0; ?>
                <?php foreach ($pages as $page) { ?>
                <?php $i++; ?>
                <tr>
                    <td class="text-center"><?= $i ?></td>
                    <td><?= $page['Title'] ?></td>
                    <td><a target="_blank" href="<?= Url::route('p/' . $page['Link']) ?>"><?= empty($page['Link']) ? '/' : $page['Link'] ?></a></td>
                    <td><?= mb_substr(strip_tags($page['Text']), 0, 70) ?>...</td>
                    <td style="width: 160px; text-align: center">
                        <a href="<?= Url::route('admin/pages/edit.php', ['id' => $page['ID']]) ?>" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</a>                       
                        <?php if ($page['System'] != 1) { ?>
                        &nbsp;
                        <a href="<?= Url::route('admin/pages/index.php', ['delID' => $page['ID']]) ?>" onclick="return confirm('Confirmați?')" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> Delete</a>                        
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                    <td colspan="5" class="text-center">
                        Nu este nici o pagina
                    </td> 
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
</div>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>