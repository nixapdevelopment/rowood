<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/admin/check.php';
require_once DOC_ROOT . '/services/Payment.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$payment_service = new Payment();

$limit = 10;
$status = isset($_GET['status']) ? $payment_service->escape($_GET['status']) : false;
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
$offset = ($page - 1) * $limit;
$status_where = $status ? "and `Status` = '$status'" : '';

$res = $payment_service->query("select count(*) as count from `Payment` where `Type` = 'Donate' $status_where");
$res = reset($res);
$all = $res['count'];

$num_pages = floor($all / $limit);

$payments = $payment_service->getAll("where `Type` = 'Donate' $status_where order by Date desc limit $limit offset $offset");

?>

<?php require_once DOC_ROOT . '/admin/template/head.php'; ?>
<?php require_once DOC_ROOT . '/admin/template/sidebar.php'; ?>

<div class="container">
    <div id="main-wrap">

        <h1>Donatori</h1>
        <hr />
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <div class="row">
            <div>
                <div>
                    <a href="?" class="btn btn-primary">Toate</a>
                    <a href="?status=Paid" class="btn btn-success">Paid</a>
                    <a href="?status=Pending" class="btn btn-warning">Pending</a>
                    <a href="?status=Canceled" class="btn btn-danger">Canceled</a>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">№ comanda</th>
                            <th>Nume, Prenume</th>
                            <th>Email</th>
                            <th>Telefon</th>
                            <th>Județ</th>
                            <th>Localitate</th>
                            <th>Data</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($payments) > 0) { ?>
                        <?php $i = $offset; ?>
                        <?php foreach ($payments as $payment) { ?>
                        <?php $i++; ?>
                        <tr>
                            <td class="text-center"><?= $payment['ID'] ?></td>
                            <td><?= $payment['FirstName'] . ' ' . $payment['LastName'] ?></td>
                            <td><?= $payment['Email'] ?></td>
                            <td><?= $payment['Phone'] ?></td>
                            <td><?= $payment['Judet'] ?></td>
                            <td><?= $payment['Localitate'] ?></td>
                            <td><?= DateHelper::toDMY($payment['Date']) ?></td>
                            <td><?= $payment['Status'] ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                            <td colspan="8" class="text-center">
                                Nu este nici o donatie
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <br />
                <div class="text-center">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <?php for ($j = 1; $j <= $num_pages; $j++) { ?>
                            <li <?= $page == $j ? 'class="active"' : '' ?>><a href="?page=<?= $j ?>&status=<?= $status ?>"><?= $j ?></a></li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once DOC_ROOT . '/admin/template/footer.php'; ?>