<?php
require_once DOC_ROOT . '/libraries/Lang.php';
?>




<header class="header">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-3">                                        
                    <div class="header-logo">
                        <a href="<?= Url::link('/admin'); ?>"><img src="<?= Url::link('assets/images/logotype.png'); ?>"></a>
                    </div>
                </div>
                <div class="col-md-4">
                    Panoul de administrare: <strong>NIXAP</strong>
                </div>

                <div class="col-md-5">                                            
                    <ul class="top-bar-nav">
                        <li><a class="red-btn" href="<?= Url::route('admin/logout.php') ?>">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-md-1">

                </div>
                <div class="header-nav pull-right col-md-11">
                    <ul>
                        <li>
                            <a href="<?= Url::route('admin/categories/index.php') ?>">Categorii</a>
                        </li>
                        <li>
                            <a href="<?= Url::route('admin/products/index.php') ?>">Produse</a>
                        </li>
                        <li>
                            <a href="<?= Url::route('admin/news/index.php') ?>">Noutați</a>
                        </li>
                        <li>
                            <a href="<?= Url::route('admin/pages/index.php') ?>">Pagini</a>
                        </li>
                        <li>
                            <a href="<?= Url::route('admin/slideshow/index.php') ?>">Slide Show</a>
                        </li>
                        <li>
                            <a href="<?= Url::route('admin/reviews/index.php') ?>">Recenzii</a>
                        </li>
                        <li>
                            <a href="<?= Url::route('admin/subscribers/index.php') ?>">Subscribers</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<p>&nbsp;</p>

<p>&nbsp;</p>