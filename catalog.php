<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/services/CategoryImage.php';
require_once DOC_ROOT . '/services/CategoryRelation.php';

$category_service = new Category();
$category_image_service = new CategoryImage();
$category_relation_service = new CategoryRelation();

if (empty($_GET['url']))
{
    Url::redirect();
}

$current_category = $category_service->getByLink($_GET['url']);

if (empty($current_category['ID']))
{
    Url::redirect();
}

$category_images = $category_image_service->query("select * from `CategoryImage` where `CategoryID` = " . $current_category['ID'] . " order by `Main` desc");

$related_categories = $category_relation_service->getRelatedCategories($current_category['ID']);

$TITLE = $current_category['Title'];
$KEYWORDS = $current_category['Keywords'];
$DESCRIPTION = $current_category['Description'];

?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<section class="home-indigo indigo-mini"></section>

<section class="content">
    <div class="bread">		
        <div class="container">
            <a href="<?= Url::route() ?>"><i class="fa fa-home"></i> Home</a> - <?= $current_category['Title'] ?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="page-title"><?= $current_category['Title'] ?> <div class="big-dot-white"></div></h2>
                <h4 class="sub-title"><?= $current_category['Subtitle'] ?></h4>
                <div>
                    <?= $current_category['Text'] ?>
                </div>
                <p>&nbsp;</p>
                <a href="#" data-toggle="modal" data-target="#offer-modal" class="cere-oferta">Cere oferta</a>
            </div>
            <div class="col-md-6">
                <?php foreach ($category_images as $img) { ?>
                <div class="portfolio-item col-md-6">
                    <div class="portfolio-thumb">
                        <a class="fancybox" rel="portfolio-thumb" href="<?= Url::link('uploads/' . $img['Image']) ?>" title="">
                            <img src="<?= Url::link('uploads/' . $img['Thumb']) ?>" alt="<?= $current_category['Title'] ?>" />
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>	
</section>

<section class="promotii violet-pattern">
    <div class="container">
        <h4>În fiecare lună te așteptăm cu promoții</h4>
        <h2 class="white-title">Promoții noi <div class="big-dot"></div></h2>
        <p>
            Lorem ipsum dolor sit amet, sanctus intellegam cu sit, his quis alii falli eu, est no posse timeam. Cu populo fabellas vix, te tantas iuvaret nam. Vidit solet vulputate sed te, ad pri essent eirmod invidunt, eos singulis maluisset in. Accusata gloriatur an quo, ius exerci feugait phaedrum an, affert disputationi vituperatoribus pri te.
        </p>
        <a href="#" data-toggle="modal" data-target="#offer-modal" class="cere-oferta">Cere oferta</a>
    </div>
</section>

<?php if (count($related_categories) > 0) { ?>
<section class="home-portfolio">
    <div class="container">
        <h2 class="green-title">Categorii asemanatoare <div class="big-dot"></div></h2>
        <div id="related-categories">
            <?php foreach ($related_categories as $item) { ?>
            <div style="margin: 15px;" class="portfolio-item">
                <a href="<?= Url::route($item['Link']) ?>" title="<?= $item['Title'] ?>">
                    <div class="portfolio-thumb">
                        <img src="<?= Url::link('uploads/' . $item['Thumb']) ?>" alt="<?= $item['Title'] ?>" />
                        <p><?= $item['Title'] ?></p>
                    </div>
                </a>
            </div>
            <?php } ?>		
        </div>

        <div class="related-categories portfolio-arrow arrow-left"></div>
        <div class="related-categories portfolio-arrow arrow-right"></div>
    </div>
</section>
<?php } ?>

<script>

    $(document).ready(function(){
        $('a[href$="<?= $current_category['Link'] ?>"]').closest('li').addClass('active');
        
        $('#offer-form select[name="Category"]').val(<?= $current_category['ID'] ?>);
    });

</script>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>