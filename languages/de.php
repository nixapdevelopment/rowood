<?php

$LANG['home'] = "Zuhause";
$LANG['contact'] = "Kontakt";
$LANG['newsletter'] = "Newsletter";
$LANG['subscribe_on_our_news'] = "Abonați-vă la noutățile noastre, pentru a fi la curent cu toate evenimentele Bisericii Negre.";
$LANG['enter_email'] = "introduce-ți adresa de email";
$LANG['footer_copyright'] = "Biserica Evanghelică C.A. România Braşov";
$LANG['footer_contact'] = "Biserica Evanghelică C.A. România <br />Parohia Braşov Curtea Johannes Honterus 2 <br />RO 500025 Braşov, România<br /><br />";
$LANG['footer_phone'] = "Telefon";
$LANG['header_welcome'] = "Willkommen auf der Webseite von der Evangelischen Kirche in Brasov";
//$LANG[''] = "";

?>

