<?php

$LANG['home'] = "Acasa";
$LANG['contact'] = "Contacte";
$LANG['newsletter'] = "Newsletter";
$LANG['subscribe_on_our_news'] = "Abonați-vă la noutățile noastre, pentru a fi la curent cu toate evenimentele Bisericii Negre.";
$LANG['enter_email'] = "introduce-ți adresa de email";
$LANG['footer_copyright'] = "RoWood";
$LANG['footer_contact'] = "RoWood România <br /><br />";
$LANG['footer_phone'] = "Telefon";
$LANG['header_welcome'] = "Bine ați venit pe pagină web a Bisericii Evanghelice din Brașov";
$LANG['order'] = "Cumpără";
$LANG['rez_title'] = 'Cumpără bilet la acest eveniment';
$LANG['buy_ticket'] = "Cumpără bilet";
$LANG['Name'] = "Nume";
$LANG['Email'] = "Email";
$LANG['Phone'] = "Telefon";
$LANG['Judet'] = "Județ";
$LANG['Location'] = "Localitate";
$LANG['PersonType'] = "Persoană";
$LANG['Tip'] = "Tip";
$LANG['NrPersons'] = "Număr de persoane";
$LANG['PaymentType'] = "Metoda de achitare";
$LANG['Ghidaj'] = "Ghid";
$LANG['Cost'] = "Cost";
$LANG['CuiCif'] = "CUI / CIF";
$LANG['NrReg'] = "Nr. înregistrare la Registrul de Comerț";
$LANG['Bank'] = "Bancă";
$LANG['Tva'] = "Plătitor de TVA";
$LANG['AtiRezervatCuSucces'] = "Ați rezervat cu succes!";
//$LANG[''] = "";

?>


