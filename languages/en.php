<?php

$LANG['home'] = "Home";
$LANG['contact'] = "Contact";
$LANG['newsletter'] = "Newsletter";
$LANG['subscribe_on_our_news'] = "Abonați-vă la noutățile noastre, pentru a fi la curent cu toate evenimentele Bisericii Negre.";
$LANG['enter_email'] = "enter your email address";
$LANG['footer_copyright'] = "Biserica Evanghelică C.A. România Braşov";
$LANG['footer_contact'] = "Biserica Evanghelică C.A. România <br />Parohia Braşov Curtea Johannes Honterus 2 <br />RO 500025 Braşov, România<br /><br />";
$LANG['footer_phone'] = "Phone";
$LANG['header_welcome'] = "Welcome to the web page of the Evangelical Church in Brasov";
//$LANG[''] = "";

?>

