<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Payment.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/payment/Mobilpay.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Email.php';

$payment_service = new Payment();

if (Request::isPost())
{
    $orderType = Request::post('OrderType') == 'Event' ? 'Event' : 'Donate';
    
    $Fullname = Validator::validate('Fullname', Validator::ValidateEmpty, 'Fullname');
    $email = Validator::validate('Email', Validator::ValidateEmail, 'Email');
    $phone = Validator::validate('Phone', Validator::ValidateEmpty, 'Phone');
    $Judet = Validator::validate('Judet', Validator::ValidateEmpty, 'Judet');
    $Localitate = Validator::validate('Localitate', Validator::ValidateEmpty, 'Location');
    $amount = Validator::validate('DonateSum', Validator::ValidateNumber, 'DonateSum');
    $PaymentType = Request::post('PaymentType');

    if (!Validator::hasErrors())
    {
        $Fullname = trim($Fullname);
        $arr = explode(' ', $Fullname);
        $firstName = reset($arr);
        $lastName = end($arr);
        $address = $Localitate . ' (' . $Judet . ')';
        
        $orderID = $payment_service->insert([
            'Type' => $orderType,
            'Amount' => $amount,
            'FirstName' => $firstName,
            'LastName' => $lastName,
            'Email' => $email,
            'Phone' => $phone,
            'Localitate' => $Localitate,
            'Judet' => $Judet,
            'Status' => Payment::StatusPending
        ]);
        
        $message = "$firstName $lastName a făcut o donație în summa $amount RON.<br><br>Contacte personei: $phone / $email";
        
        Email::send(Config::$OrderEmail, 'Donatie la ' . Url::route(), $message);
        $_SESSION['is_donate'] = true;
        if ($PaymentType == 'Card')
        {
            Mobilpay::process($orderID, $amount, $returnUrl, $firstName, $lastName, $address, $email, $phone);
        }
        else
        {
            $_SESSION['PaypalOrderID'] = $orderID;
        ?>
        <form id="paypal-form" action="https://www.sandbox.paypal.com/cgi-bin/websc" method="post">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="colesnic89@gmail.com">
            <input id="paypalItemName" type="hidden" name="item_name" value="Donatie pentru Biserica Neagra">
            <input id="paypalQuantity" type="hidden" name="quantity" value="1">
            <input id="paypalAmmount" type="hidden" name="amount" value="<?php echo number_format($amount / 4.5, 2); ?>">
            <input type="hidden" name="no_shipping" value="1">
            <input type="hidden" name="return" value="<?php echo Url::route('payment_confirm', ['orderId' => $orderID]); ?>">

            <input type="hidden" name="custom" value="{}">

            <input type="hidden" name="currency_code" value="EUR">
            <input type="hidden" name="lc" value="RO">
            <input type="hidden" name="bn" value="PP-BuyNowBF">
        </form>
        <script>document.getElementById("paypal-form").submit();</script>
        <?php
        exit;
        }
    }
    
    echo Validator::showMessages(true);
}
else
{
    exit('POST REQUEST REQUIRED');
}