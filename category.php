<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/services/CategoryLang.php';
require_once DOC_ROOT . '/services/CategoryImage.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$category_service = new Category();
$category_lang_service = new CategoryLang();
$category_image_service = new CategoryImage();

if (empty($_GET['url'])) {
    Url::redirect();
}

$current_article = $category_lang_service->getByLink($_GET['url']);

if (empty($current_article['CategoryID'])) {
    Url::redirect();
}

$subcategories = $category_service->getAll("WHERE ParentID = " . $current_article['CategoryID']);

$news_list = $category_service->getRelatedNews($current_article['CategoryID']);

$images = $category_image_service->getWhere([
    'CategoryID' => $current_article['CategoryID']
]);

$TITLE = $current_article['Title'];
$KEYWORDS = $current_article['Keywords'];
$DESCRIPTION = $current_article['Description'];
?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<section class="content" style="margin-top: 150px;">
    <div class="bread">		
        <div class="container">
            <a href="<?= Url::route() ?>"><i class="fa fa-home"></i> Home</a> - <?= $current_article['Title'] ?>
        </div>
    </div>
    <div class="container">
        <h2 class="page-title"><?= $current_article['Title'] ?> <div class="big-dot-green"></div></h2>
        <?php if (count($subcategories) > 0) { ?>
        <hr />
        <div class="row">
            <?php foreach ($subcategories as $cat) { ?>
            <div class="col-md-3">
                <a style="font-weight: bolder; color: #000;" href="<?= Url::route('c/' . $cat['Link']) ?>" title="<?= $cat['Title'] ?>"><?= $cat['Title'] ?></a>
            </div>
            <?php } ?>
        </div>
        <hr />
        <?php } ?>
        <p><?= $current_article['Text'] ?></p>
        <div class="row">
            <?php if (count($news_list) > 0) { ?>
                <?php foreach ($news_list as $news) { ?>                        
                    <div class="col-md-6">
                        <a class="event-item" href="<?= Url::route('news/' . $news['Link']) ?>" title="<?= $news['Title'] ?>">
                            <time><strong><?= DateHelper::toD($news['Date']) ?></strong><br /><?= DateHelper::toM($news['Date']) ?><br /><?= DateHelper::toY($news['Date']) ?></time>
                            <div class="event-item-short">
                                <strong><?= mb_substr(strip_tags($news['Title']), 0, 40) ?></strong>
                                <p><?= mb_substr(strip_tags($news['Text']), 0, 50) ?></p>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>  
        <div class="row">
            <?php if (count($images) > 0) { ?>
                <?php foreach ($images as $img) { ?>                        
                    <div class="col-md-3">
                        <a class="lightbox" href="<?= Url::link('uploads/' . $img['Image']) ?>">
                            <img class="img-thumbnail" src="<?= Url::link('uploads/' . $img['Thumb']) ?>" />
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>  
    </div>
</div>	
</section>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>