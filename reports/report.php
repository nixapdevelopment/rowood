<?php

require $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';

$orderID = isset($_GET['order']) ? $_GET['order'] : [];

require $_SERVER['DOCUMENT_ROOT'] . '/libraries/tcpdf/tcpdf.php';

$tcPdf = new TCPDF('P', 'mm', 'A4', true, 'utf-8');

$tcPdf->setPrintHeader(false);
$tcPdf->setPrintFooter(false);
$tcPdf->SetMargins(5, 0, 5);
$tcPdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcPdf->SetFont('opensans', '', 12);

$tcPdf->AddPage();

$tcPdf->SetTitle('Factura');
$tcPdf->setPageOrientation('P');
ob_start();
require_once 'template/factura.php';
$content = ob_get_clean();

$tcPdf->writeHTML($content, true, false, false, false, '');

$tcPdf->Output('Factura' . '.pdf', 'I');