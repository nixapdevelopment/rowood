<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Payment.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/PaymentEvent.php';

$payment_service = new Payment();
$payment_event_service = new PaymentEvent();

$payment = $payment_service->getByID($orderID);

$res = $payment_event_service->query("select * from PaymentEvent as pe left join Event as e on e.ID = pe.EventID left join EventLang as el on el.EventID = e.ID and el.LangID = " . Request::getLangID() . " where pe.PaymentID = " . $orderID);
$event = reset($res);
?>

<style>
    .list td, .list th {
        border: 1px solid #000000;
        text-align: center;
    }
</style>

<table style="font-size: 10px;">
    <tr>
        <td>
            <h3>Producator</h3>
            <hr />
            <div>
                Biserica Evanghelică C.A.<br>
                România, Braşov, Curtea Johannes Honterus 2<br>
                RO 500025 Braşov, România<br>
                Telefon: 0040 (0)268 511824<br>
                Fax: 0040 (0)268 511825<br>
                E-mail: info@biserica-neagra.ro<br>
                <br>
                Cont IBAN EUR: RO90 RZBR 0000 0600 0268 4897<br>
                Swift: RZBRROBU<br>
                Raiffeisen Bank Agenţia Piaţa Sfatului Braşov
            </div>
        </td>
        <td>
            <h3>Cumparator</h3>
            <hr />
            <div>
                <?= $payment['FirstName'] ?> <?= $payment['LastName'] ?><br>
                jud. <?= $payment['Judet'] ?>, <?= $payment['Localitate'] ?><br>
                Telefon: <?= $payment['Phone'] ?><br>
                E-mail: <?= $payment['Email'] ?><br>
            </div>
        </td>
    </tr>
</table>
<br>
<hr />
<div>
    <br>
</div>
<div style="text-align: center">
    Factura № <?= str_pad($orderID, 7, '0', STR_PAD_LEFT) ?><br>
    din <?= date('d.m.Y', strtotime($payment['Date'])) ?>
</div>
<br />

<table valign="middle" style="width: 485px; margin-top: 20px; font-size: 11px;" cellpadding="4" class="list">
    <thead>
        <tr>
            <th style="width:200px;">Eveniment</th>
            <th>Bilete</th>
            <th>Preț</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="width:200px;"><?= $event['Title'] ?></td>
            <td><?= $event['Tickets'] ?></td>
            <td><?= number_format($payment['Amount'] / $event['Tickets'], 2) ?></td>
            <td><?= $payment['Amount'] ?></td>
        </tr>
        <?php if ($event['UseGuide']) { ?>
        <tr>
            <td style="width:200px;">Servicii ghid</td>
            <td>-</td>
            <td><?= $event['GuidePrice'] ?></td>
            <td><?= $event['GuidePrice'] ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<br />
<div align="right">
    TOTAL SPRE PLATA: <b style="font-size: 16px;"><?= $event['UseGuide'] ? $payment['Amount'] + $event['GuidePrice'] : $payment['Amount'] ?></b> RON
</div>
<br />
<br />
