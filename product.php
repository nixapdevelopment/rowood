<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/Event.php';
require_once DOC_ROOT . '/services/EventImage.php';
require_once DOC_ROOT . '/services/EventInfo.php';
require_once DOC_ROOT . '/services/EventSpec.php';
require_once DOC_ROOT . '/services/EventNews.php';

$event_service = new Event();
$event_image_service = new EventImage();
$event_info_service = new EventInfo();
$event_spec_service = new EventSpec();
$event_news_service = new EventNews();

$product = $event_service->getByLink($_GET['link']);

if (empty($product['ID']))
{
    Url::redirect();
}

$product_images = $event_image_service->getWhere(['EventID' => $product['ID']]);

$product_info = $event_info_service->getWhere(['EventID' => $product['ID'], 'LangID' => Request::getLangID()]);

$product_spec = $event_spec_service->getByProductID($product['ID']);

$product_news = $event_news_service->getByProductID($product['ID']);

?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<div class="uk-clearfix banner">
    <div class="page_headings">
        <div class="table">
            <div>
                <h2>
                    <?= $product['Title'] ?>
                </h2>
                <p>
                    <a style="color: #fff;" href="<?= Url::route() ?>">Home</a> - <?= $product['Title'] ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="uk-clearfix section">
    <div class="uk-container uk-container-center page_content">
        <div class="uk-grid thumb_slider">
            <div class="uk-width-medium-1-2">
                <ul class="bxslider">
                    <?php foreach ($product_images as $image) { ?>
                    <li>
                        <a href="#">
                            <img src="<?= Url::link('uploads/' . $image['Image']) ?>" alt="" />
                        </a>
                    </li>
                    <?php } ?>
                </ul>

                <div class="uk-clearfix mt15 pager">
                    <div id="bx-pager" class="uk-clearfix">
                        <?php foreach ($product_images as $ind => $image) { ?>
                        <a data-slide-index="<?= $ind ?>">
                            <img style="max-height: 100px;" src="<?= Url::link('uploads/' . $image['Thumb']) ?>" />
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="uk-clearfix product_inner inside">
                    <h2>
                        <?= $product['Title'] ?>
                    </h2>
                    <h5>
                        Descriere
                    </h5>
                    <p>
                        <?= $product['Text'] ?>
                    </p>

                    <div class="uk-clearfix details noborder">
                        <?php if (!empty($product_info)) { ?>
                        <?php foreach ($product_info as $info) { ?>
                        <div class="table">
                            <div>
                                <img src="<?= Url::link('uploads/event-info/' . $info['Icon']) ?>" alt="" />
                            </div>
                            <div>
                                <h5>
                                    <?= $info['Title'] ?>
                                </h5>
                                <span>
                                    <?= $info['Subtitle'] ?>
                                </span>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    
                    <br />
                    <div class="uk-clearfix socials">
                        <script src="http://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="http://yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="facebook,gplus,twitter,linkedin" data-counter=""></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-clearfix mt40">
            <div class="title">
                <h5>
                    <h5>
                        Specificatii tehnice
                    </h5>
                </h5>
            </div>

            <div class="uk-clearfix">
                <div class="uk-grid">
                    <?php foreach ($product_spec as $table) { ?>
                    <div class="uk-width-medium-1-3 mt10">
                        <table class="uk-table uk-table-striped uk-width-1-1">
                            <tr>
                                <td>Specificatie</td>
                                <td class="uk-text-center">Valoare</td>
                            </tr>
                            <?php foreach ($table as $row) { ?>
                            <tr>
                                <td><?= $row['Name'] ?></td>
                                <td class="uk-text-center"><?= $row['Value'] ?></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (count($product_news) > 0) { ?>
<div class="uk-clearfix gray">
    <div class="uk-container uk-container-center">
        <div class="uk-grid some_more">
            <?php foreach ($product_news as $news) { ?>
            <div class="uk-width-medium-1-3">
                <div class="item">
                    <a href="<?= Url::route('news/' . $news['Link']) ?>">
                        <img src="<?= Url::link('assets/images/p1.jpg') ?>" alt="<?= $news['Title'] ?>" />
                    </a>
                    <h3>
                        <?= $news['Title'] ?>
                    </h3>
                    <p>
                        <?= mb_substr(strip_tags($news['Text']), 0, 100) ?>
                    </p>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>