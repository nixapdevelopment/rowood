<?php

require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/services/Event.php';

$category_service = new Category();
$event_service = new Event();

$categories = $category_service->getAll();
$products = $event_service->getList();

?>

<div class="uk-clearfix section">
    <div class="uk-container uk-container-center page_content">
        <div class="uk-text-center">
            <?= $current_article['Text'] ?>
        </div>
    </div>
</div>

<div class="uk-clearfix section">
    <div class="uk-container uk-container-center">
        <!-- FILTER CONTROLS -->
        <div class="uk-grid tab_mix">
            <div class="uk-width-medium-1-4">
                <a href="#" class="filter" data-filter="all">TOATE</a>
            </div>
            <?php foreach ($categories as $category) { ?>
            <div class="uk-width-medium-1-4">
                <a href="#" class="filter" data-filter="category_<?= $category['ID'] ?>"><?= $category['Title'] ?></a>
            </div>
            <?php } ?>
        </div>

        <div class="uk-clearfix">
            <!-- GRID -->
            <ul id="mixes" class="uk-clearfix">
                <?php foreach ($products as $product) { ?>
                <li class="mix category_<?= $product['data']['CategoryID'] ?>" data-cat="<?= $product['data']['CategoryID'] ?>">
                    <div class="uk-clearfix product_inner">
                        <div class="uk-clearfix image">
                            <a href="<?= Url::route('product/' . $product['data']['Link']) ?>">
                                <img src="<?= Url::link('uploads/' . $product['data']['Image']) ?>" alt="<?= $product['data']['Title'] ?>">
                            </a>
                        </div>
                        <div class="content uk-text-center">
                            <a href="<?= Url::route('product/' . $product['data']['Link']) ?>">
                                <h3>
                                    <?= $product['data']['Title'] ?>
                                </h3>
                            </a>
                        </div>
                        <div class="uk-clearfix details">
                            <?php if (!empty($product['info'])) { ?>
                            <?php foreach ($product['info'] as $info) { ?>
                            <div class="table">
                                <div>
                                    <img src="<?= Url::link('uploads/event-info/' . $info['InfoIcon']) ?>" alt="" />
                                </div>
                                <div>
                                    <h5>
                                        <?= $info['InfoTitle'] ?>
                                    </h5>
                                    <span>
                                        <?= $info['InfoSubtitle'] ?>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>