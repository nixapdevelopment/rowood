<?php
require_once 'includes.php';
require_once DOC_ROOT . '/core/core.php';
require_once DOC_ROOT . '/services/News.php';
require_once DOC_ROOT . '/services/Category.php';
require_once DOC_ROOT . '/services/CategoryImage.php';
require_once DOC_ROOT . '/libraries/DateHelper.php';

$news_service = new News();
$category_service = new Category();
$category_image_service = new CategoryImage();

if (empty($_GET['url']))
{
    Url::redirect();
}

$current_article = $news_service->getByLink($_GET['url']);

if (empty($current_article['ID']))
{
    Url::redirect();
}

$rand_categories = $category_service->getAllWithImages(3, true);

$TITLE = $current_article['Title'];
$KEYWORDS = $current_article['Keywords'];
$DESCRIPTION = $current_article['Description'];

?>

<?php require_once DOC_ROOT . '/template/head.php'; ?>

<section class="home-indigo indigo-mini"></section>

<section class="content">
    <div class="bread">		
        <div class="container">
            <a href=""><i class="fa fa-home"></i> Home</a> - <?= $current_article['Title'] ?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h2 class="page-title"><?= $current_article['Title'] ?> <div class="big-dot-green"></div></h2>
                <h4><?= DateHelper::toDMY($current_article['Date']) ?></h4>
                <div>
                    <?= $current_article['Text'] ?>
                </div>
            </div>
            <div class="col-md-3">					
                <?php foreach ($rand_categories as $item) { ?>
                <div style="margin: 15px;" class="portfolio-item">
                    <a href="<?= Url::route($item['Link']) ?>" title="<?= $item['Title'] ?>">
                        <div class="portfolio-thumb">
                            <img src="<?= Url::link('uploads/' . $item['Thumb']) ?>" alt="<?= $item['Title'] ?>" />
                            <p><?= $item['Title'] ?></p>
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>	
</section>

<section class="promotii green-pattern">
    <div class="container">
        <h4>În fiecare lună te așteptăm cu promoții</h4>
        <h2 class="white-title">Promoții noi <div class="big-dot"></div></h2>
        <p>
            Lorem ipsum dolor sit amet, sanctus intellegam cu sit, his quis alii falli eu, est no posse timeam. Cu populo fabellas vix, te tantas iuvaret nam. Vidit solet vulputate sed te, ad pri essent eirmod invidunt, eos singulis maluisset in. Accusata gloriatur an quo, ius exerci feugait phaedrum an, affert disputationi vituperatoribus pri te.
        </p>
        <a href="#" data-toggle="modal" data-target="#offer-modal" class="cere-oferta">Cere oferta</a>
    </div>
</section>

<?php require_once DOC_ROOT . '/template/footer.php'; ?>